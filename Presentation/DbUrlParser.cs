﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Presentation
{
    public static class DbUrlParser
    {
        public static string Parse(string url)
        {
            string connUrl = null;

            if (url.Contains("URL"))
            {
                connUrl = Environment.GetEnvironmentVariable(url);
                connUrl = connUrl.Replace("postgres://", string.Empty);
            }
            else
            {
                connUrl = url.Replace("postgres://", string.Empty);
            }

            string pgUserPass = connUrl.Split("@")[0];
            string pgHostPortDb = connUrl.Split("@")[1];
            string pgHostPort = pgHostPortDb.Split("/")[0];
            string pgDb = pgHostPortDb.Split("/")[1];
            string pgUser = pgUserPass.Split(":")[0];
            string pgPass = pgUserPass.Split(":")[1];
            string pgHost = pgHostPort.Split(":")[0];
            string pgPort = pgHostPort.Split(":")[1];

            return $"Server={pgHost};Port={pgPort};User Id={pgUser};Password={pgPass};Database={pgDb};sslmode=Require;Trust Server Certificate=true";
        }
    }
}
