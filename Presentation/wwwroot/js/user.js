﻿//Send sign up data to action method and reload page if user is succefully signed up.
$('#signup-submit').click(function (e) {
    var data = {
        username: $('#signup-username').val(),
        email: $('#signup-email').val(),
        password: $('#signup-password').val(),
        confirmPassword: $('#signup-confirm-password').val(),
        rememberMe: $('#signup-rememberMe').prop('checked')
    }

    var jsonData = JSON.stringify(data);

    $('#signup-username').prop('disabled', true);
    $('#signup-email').prop('disabled', true);
    $('#signup-password').prop('disabled', true);
    $('#signup-confirm-password').prop('disabled', true);
    $('#signup-rememberMe').prop('disabled', true);
    $('#signup-submit').prop('disabled', true);
    $('#signup-google').prop('disabled', true);
    $('#signup-facebook').prop('disabled', true);
    $('#signup-forgot').prop('disabled', true);

    $.ajax({
        url: '/User/Signup',
        type: 'POST',
        dataType: 'json',
        data: jsonData,
        contentType: 'application/json;charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            var resultData = JSON.parse(result);
            if (resultData.Succedded) {
                $("#signup-error-message").empty();
                $("#signup-confirmation-message").html(resultData.Message);

                $('#signup-username').prop('disabled', false);
                $('#signup-email').prop('disabled', false);
                $('#signup-password').prop('disabled', false);
                $('#signup-confirm-password').prop('disabled', false);
                $('#signup-rememberMe').prop('disabled', false);
                $('#signup-submit').prop('disabled', false);
                $('#signup-google').prop('disabled', false);
                $('#signup-facebook').prop('disabled', false);
                $('#signup-forgot').prop('disabled', false);

                $('#signup-username').val("");
                $('#signup-email').val("");
                $('#signup-password').val("");
                $('#signup-confirm-password').val("");
                $('#signup-rememberMe').val("");
            } else {
                $("#signup-confirmation-message").empty();
                $("#signup-error-message").html(resultData.Message);

                $('#signup-username').prop('disabled', false);
                $('#signup-email').prop('disabled', false);
                $('#signup-password').prop('disabled', false);
                $('#signup-confirm-password').prop('disabled', false);
                $('#signup-rememberMe').prop('disabled', false);
                $('#signup-submit').prop('disabled', false);
                $('#signup-google').prop('disabled', false);
                $('#signup-facebook').prop('disabled', false);
                $('#signup-forgot').prop('disabled', false);
            }
        },
        error: function () {
        }
    });
    e.preventDefault();
});

//Disable sign up button if fields have failed client-side or server side validation.
$(document).ready(function () {
    var isUsernameValid = false;
    var isEmailValid = false;
    var isPasswordValid = false;
    var isConfirmPasswordValid = false;

    $('.signup-input').on('keydown paste blur cut', function (e) {

        setTimeout(function () {

            if (e.target.id == 'signup-username') {

                if ($('#signup-username').valid()) {

                    $.ajax({
                        url: '/check-username',
                        type: 'GET',
                        dataType: 'json',
                        async: false,
                        data: { username: $('#signup-username').val() },
                        success: function (result) {

                            if (result == true) {
                                isUsernameValid = true;
                                $('#username-error-message').empty();
                            } else {
                                isUsernameValid = false;
                                $('#username-error-message').html('Username already taken.');
                            }
                        },
                        error: function (error) {
                            window.location.href = '../';
                        }
                    });

                } else {
                    isUsernameValid = false;
                    $('#username-error-message').empty();
                }

            } else if (e.target.id == 'signup-email') {

                if ($('#signup-email').valid()) {

                    $.ajax({
                        url: '/check-email',
                        type: 'GET',
                        dataType: 'json',
                        async: false,
                        data: { email: $('#signup-email').val() },
                        success: function (result) {

                            if (result == true) {
                                isEmailValid = true;
                                $('#email-error-message').empty();
                            } else {
                                isEmailValid = false;
                                $('#email-error-message').html('Email already in use.');
                            }
                        },
                        error: function () {
                            window.location.reload();
                        }
                    });

                } else {
                    isEmailValid = false;
                    $('#email-error-message').empty();
                }

            } else if (e.target.id == 'signup-password') {

                if ($('#signup-password').valid()) {

                    if ($('#signup-password').val() == $('#signup-confirm-password').val()) {
                        isPasswordValid = true;
                        isConfirmPasswordValid = true;
                        $('#signup-confirm-password-error').empty();
                    } else {
                        $('#signup-confirm-password').valid();
                        isPasswordValid = false;
                    }

                } else {
                    isPasswordValid = false;
                }

            } else if (e.target.id == 'signup-confirm-password') {

                if ($('#signup-confirm-password').valid()) {

                    if ($('#signup-password').val() == $('#signup-confirm-password').val()) {
                        isPasswordValid = true;
                        isConfirmPasswordValid = true;
                        $('#signup-confirm-password-error').empty();
                    } else {
                        isConfirmPasswordValid = false;
                    }

                } else {
                    isConfirmPasswordValid = false;
                }
            }

            if (isConfirmPasswordValid && isEmailValid && isPasswordValid && isUsernameValid) {
                $('.signup-submit').removeAttr('disabled');
            } else {
                $('.signup-submit').attr('disabled', 'disabled');
            }
        }, 50);
    });
});

//Toggle sign up remember me.
$(document).ready(function () {
    $('.signup-rememberMe').click(function () {
        $remember = $('.signup-rememberMe');

        if ($remember.attr('checked')) {
            $remember.removeAttr('checked');
        } else {
            $remember.attr('checked', 'checked');
        }
    });
});


//Send log in data to action method and reload page if user is succefully logged in.
$('#login-submit').click(function (e) {
    var data = {
        username: $('#login-username').val(),
        password: $('#login-password').val(),
        rememberMe: $('#login-rememberMe').prop('checked'),
        returnUrl: $('#login-returnUrl').val()
    }

    var jsonData = JSON.stringify(data);

    $('#login-username').prop('disabled', true);
    $('#login-password').prop('disabled', true);
    $('#login-rememberMe').prop('disabled', true);
    $('#login-submit').prop('disabled', true);

    $.ajax({
        url: '/login-user',
        type: 'POST',
        dataType: 'json',
        data: jsonData,
        async: false,
        contentType: 'application/json;charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            var resultData = JSON.parse(result);

            if (resultData.Succedded) {
                if (resultData.ReturnUrl != null) {
                    $('#login-username').prop('disabled', false);
                    $('#login-password').prop('disabled', false);
                    $('#login-rememberMe').prop('disabled', false);
                    $('#login-submit').prop('disabled', false);
                    window.location = '..' + resultData.ReturnUrl;
                } else {
                    $('#login-username').prop('disabled', false);
                    $('#login-password').prop('disabled', false);
                    $('#login-rememberMe').prop('disabled', false);
                    $('#login-submit').prop('disabled', false);
                    window.location.href = '../';
                }
            } else {
                $("#login-error-message").html(resultData.Message);

                $('#login-username').prop('disabled', false);
                $('#login-password').prop('disabled', false);
                $('#login-rememberMe').prop('disabled', false);
                $('#login-submit').prop('disabled', false);
            }
        },
        error: function (result) {
            $('#login-username').prop('disabled', false);
            $('#login-password').prop('disabled', false);
            $('#login-rememberMe').prop('disabled', false);
            $('#login-submit').prop('disabled', false);
            window.location.href = '../';
        }
    });
    e.preventDefault();
});

//Disable log in button if fields have failed client-side validation.
$(document).ready(function () {
    var isUserNameValid = false;
    var isPasswordValid = false;

    $('.login-input').on('keydown blur paste cut', function (e) {

        setTimeout(function () {

            if (e.target.id == 'login-username') {

                if ($('#login-username').valid()) {
                    isUserNameValid = true;
                } else {
                    isUserNameValid = false;
                }

            } else if (e.target.id == 'login-password') {

                if ($('#login-password').valid()) {
                    isPasswordValid = true;
                } else {
                    isPasswordValid = false;
                }
            }

            if (isUserNameValid && isPasswordValid) {
                $('.login-submit').removeAttr('disabled');
            } else {
                $('.login-submit').attr('disabled', 'disabled');
            }
        }, 50);
    });
});

//Toggle login remember me.
$(document).ready(function () {
    $('.login-rememberMe').click(function () {
        $remember = $('.login-rememberMe');

        if ($remember.attr('checked')) {
            $remember.removeAttr('checked');
        } else {
            $remember.attr('checked', 'checked');
        }
    });
});

//Removes Modal window.
$(document).ready(function () {
    $('.modal-cancel').click(function (e) {
        e.preventDefault();
        document.getElementById('modal01').style.display = 'none';
        $('html').css('overflow-y', 'scroll');
    });
});

//Send email for which password is forgot
$(document).ready(function () {
    $('.reset-email-submit').click(function (e) {
        $('.reset-email-submit').attr('disabled', 'disabled');

        $.ajax({
            url: '/reset-password-email',
            type: 'GET',
            dataType: 'json',
            async: true,
            data: { email: $('#reset-email-input').val() },
            success: function (result) {
                document.getElementById('reset-email-input').value = '';
            },
            error: function () {
                document.getElementById('reset-email-input').value = '';
            }
        });
        e.stopImmediatePropagation();
    });
});


//Toggle disable for send forgot password email
$(document).ready(function () {
    var isEmailValid = false;

    $('.reset-email').on('keydown blur paste cut', function (e) {

        setTimeout(function () {

            if (e.target.id == 'reset-email-input') {

                if ($('#reset-email-input').valid()) {
                    isEmailValid = true;
                } else {
                    isEmailValid = false;
                }
            }

            if (isEmailValid) {
                $('.reset-email-submit').removeAttr('disabled');
            } else {
                $('.reset-email-submit').attr('disabled', 'disabled');
            }
        }, 50);
    });
});

//Send new password 
$(document).ready(function () {
    $('.reset-code-submit').click(function (e) {
        $('.reset-code-submit').attr('disabled', 'disabled');

        $.ajax({
            url: '/reset-password-code',
            type: 'GET',
            dataType: 'json',
            async: true,
            data: { code: $('#reset-code-submit').val() },
            success: function (result) {
                document.getElementById('reset-code-submit').value = '';
            },
            error: function () {
                document.getElementById('reset-code-submit').value = '';
            }
        });
        e.stopImmediatePropagation();
    });
});

//Toggle disable for send forgot password code
$(document).ready(function () {
    var isCodeValid = false;

    $('.reset-code').on('keydown blur paste cut', function (e) {

        setTimeout(function () {
            if (e.target.id == 'reset-code-submit') {

                if ($('#reset-code-submit').valid()) {
                    isCodeValid = true;
                } else {
                    isCodeValid = false;
                }
            }

            if (isCodeValid) {
                $('.reset-code-submit').removeAttr('disabled');
            } else {
                $('.reset-code-submit').attr('disabled', 'disabled');
            }
        }, 50);
    });
});

$(document).ready(function () {
    setTimeout(function () {
        $('#signup-username').val("");
        $('#signup-email').val("");
        $('#signup-password').val("");
        $('#signup-confirm-password').val("");
        $("#signup-error-message").empty();
        $("#username-error-message").empty();
    }, 200);
});

