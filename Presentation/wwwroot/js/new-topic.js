﻿
//Set category value, refresh button, close dropdown
function getCategoryValue(category) {
    document.getElementById('new-topic-category').value = category;
    //document.getElementById('new-topic-categories-dropdown-btn').innerHTML = category;
    //document.getElementById('new-topic-categories-dropdown').classList.remove('dropdown--open');
}

function setSelected(category) {
    category.setSelected();
}

$(document).ready(function () {
    var error = document.getElementById('new-topic-error-message');
    if (error.innerHTML != null) {
        $('.new-topic-submit').removeAttr('disabled');
    }
});

//Check if topic is valid and enable send button
$(document).ready(function () {
    var isTitleValid = false;
    var isDescriptionValid = false;

    $('.create-new-topic-helper').on('keydown paste blur cut', function (e) {

        setTimeout(function () {

            if (e.target.id == 'new-topic-title') {

                if ($('#new-topic-title').valid()) {
                    var titleField = $('#new-topic-title').val();

                    for (var i = 0; i < titleField.length; i++) {
                        if (titleField[i] != ' ') {
                            isTitleValid = true;
                            break;
                        }
                        if (titleField.length) {
                            isTitleValid = false;
                        }
                    }
                } else {
                    isTitleValid = false;
                }
            } else if (e.target.id == 'new-topic-description') {

                if ($('#new-topic-description').valid()) {

                    var desriptionField = $('#new-topic-description').val();
                    for (var i = 0; i < desriptionField.length; i++) {
                        if (desriptionField[i] != ' ') {
                            isDescriptionValid = true;
                            break;
                        }
                        if (desriptionField.length) {
                            isDescriptionValid = false;
                        }
                    }
                    
                } else {
                    isDescriptionValid = false;
                }
            }

            if (isTitleValid && isDescriptionValid) {
                $('.new-topic-submit').removeAttr('disabled');
            } else {
                $('.new-topic-submit').attr('disabled', 'disabled');
            }
        }, 50);
    });
});