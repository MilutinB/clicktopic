﻿function expandRepliedToComment(commentId) {
    //alert(commentId);
}

function expandCommentReplies(commentId) {
    var element = document.getElementById(commentId);
    if (element.classList.contains('toggle-chev-02')) {
        var replies = document.getElementsByName(commentId)[0];
        if (replies.style.maxHeight) {
            replies.style.maxHeight = null;
        } else {
            replies.style.maxHeight = replies.scrollHeight + "px";
        }
    }
}

function closeCommentReplies(commentId) {
    var element = document.getElementById(commentId);
    if (element != null) {

        if (element.classList.contains('toggle-chev-02')) {
            var replies = document.getElementsByName(commentId)[0];
            if (replies.style.maxHeight) {
                replies.style.maxHeight = null;
            }
        }
    }
}

function scrollToElement(commentId) {
    var element = $("div[id='" + commentId + "']");
    $('html,body').animate({ scrollTop: element.offset().top - window.innerHeight / 2 }, 'slow').after(document.cookie = "CLICK_TOPIC_SCROLL_TO_REPLY=0;-1; path=/");
    
}

$(document).ready(function () {
    var collapsibleReplies = document.getElementsByClassName('collapsible-reply-content');
    for (var i = 0; i < collapsibleReplies.length; i++) {
        if (collapsibleReplies[i].scrollHeight < 45) {
            var split = collapsibleReplies[i].id.split('content');
            document.getElementById('collapsible-reply-content-button' + split[1]).style.visibility = 'hidden';
        } else {
            var split = collapsibleReplies[i].id.split('content');
            var header = document.getElementById('collapsible-reply-header' + split[1]).offsetWidth;
            var buttons = document.getElementById('collapsible-reply-buttons' + split[1]).offsetWidth;
            var reply = document.getElementById('collapsible-reply' + split[1]).offsetWidth;
            $('#' + collapsibleReplies[i].id).width(reply - header - buttons - 50);
        }
    }
});

$(document).ready(function () {
    var coll = document.getElementsByClassName("toggle-chev-01");
    for (var i = 0; i < coll.length; i++) {

        coll[i].addEventListener("click", event => {
            var content = document.getElementsByClassName("collapsible-reply-content");
            var coll2 = document.getElementsByClassName("toggle-chev-01");

            for (var j = 0; j < content.length; j++) {

                if (content[j].style.maxHeight && event.currentTarget.id == coll2[j].id) {
                    content[j].style.maxHeight = null;
                } else if (event.currentTarget.id == coll2[j].id) {
                    content[j].style.maxHeight = content[j].scrollHeight + "px";
                }
            }
        });
    }
});

$(document).ready(function () {
    var isReplyValid = false;

    $('.reply-text-area').on('keydown paste blur cut', function (e) {

        setTimeout(function () {

            if ($('#' + e.target.id).valid()) {
                var replyField = $('#' + e.target.id).val();
                for (var i = 0; i < replyField.length; i++) {
                    if (replyField[i] != ' ') {
                        isReplyValid = true;
                        break;
                    }
                    if (replyField.length) {
                        isReplyValid = false;
                    }
                }
            } else {
                isReplyValid = false;
            }

            var buttonId = 'reply-text-submit' + e.target.id.split('area')[1];

            if (isReplyValid) {
                $('#' + buttonId).removeAttr('disabled');
            } else {
                $('#' + buttonId).attr('disabled', 'disabled');
            }
        }, 50);

        e.stopImmediatePropagation();
    });
});

$('.bookmark-icon').on('click', function (e) {
    var id = e.target.id.split('-')[2];
    $.ajax({
        url: '/toggle-topic-bookmarks',
        type: 'GET',
        data: { id: id },
        success: function (result) {
            if (result == true) {
                var icon = document.getElementById('bookmark-icon-' + id);
                if (icon.classList.contains('fal')) {
                    icon.classList.remove('fal');
                    icon.classList.add('fa');
                    var outerHtml = document.getElementById('bookmark-' + id);
                    outerHtml.title = 'Unfollow this topic';
                } else {
                    icon.classList.remove('fa');
                    icon.classList.add('fal');
                    var outerHtml = document.getElementById('bookmark-' + id);
                    outerHtml.title = 'Follow this topic';
                }
            }
        },
        error: function () {
        }
    });
    e.preventDefault();
});

$('.notification-icon').on('click', function (e) {
    var id = e.target.id.split('-')[2];
    $.ajax({
        url: '/toggle-topic-notifications',
        type: 'GET',
        data: { id: id },
        success: function (result) {
            if (result == true) {
                var icon = document.getElementById('notification-icon-' + id);
                if (icon.classList.contains('fal')) {
                    icon.classList.remove('fal');
                    icon.classList.add('fa');
                    var outerHtml = document.getElementById('notification-' + id);
                    outerHtml.title = 'Turn off notifications';
                } else {
                    icon.classList.remove('fa');
                    icon.classList.add('fal');
                    var outerHtml = document.getElementById('notification-' + id);
                    outerHtml.title = 'Turn on notifications';
                }
            }
        },
        error: function () {
        }
    });
    e.preventDefault();
});

$(document).ready(function () {
    var id = getCookieValue('CLICK_TOPIC_SCROLL_TO_REPLY');
    scrollToElement("main-comment-" + id);
});

function getCookieValue(a) {
    var b = document.cookie.match('(^|;)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
}

$(document).ready(function () {
    var cookieValue = getCookieValue('CLICK_TOPIC_SCROLL_TO_BOTTOM');

    if (cookieValue == 'yes') {
        $('html,body').animate({ scrollTop: document.body.scrollHeight }, 'slow').after(document.cookie = "CLICK_TOPIC_SCROLL_TO_BOTTOM=0;-1; path=/");;
    }
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 800) {
        $('#to-top').fadeIn();
    } else {
        $('#to-top').fadeOut();
    }
});

function scrollToTop() {
    $('html,body').animate({ scrollTop: 0 }, 'slow');
}