﻿$('.close-welcome').click(function (e) {

    var jsonData = JSON.stringify(true);

    $.ajax({
        url: '/set-welcome-cookie',
        type: 'GET',
        dataType: 'json',
        data: jsonData,
        contentType: 'application/json;charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            document.getElementById("welcomeMessage").outerHTML = "";
        },
        error: function (e) {
            
        }
    });
    e.preventDefault();
});

