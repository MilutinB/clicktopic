﻿$('.notification').click(function (e) {
    var id = jQuery(this).attr("id").split('-')[1];

    $.ajax({
        url: '/set-scroll-to-reply-cookie',
        type: 'GET',
        data: { topicId: $('input#notification-topic-' + id).val(), replyId: $('input#notification-reply-' + id).val()},
        async: false,
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            
        },
        error: function (result) {
        }
    });
});


