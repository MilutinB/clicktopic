﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/send-notifications").build();

connection.start();

connection.on("ReceiveNotification", function (topicId, replyId, dateCreated, photoPath, username, topicTitle) {
    var notificationsCount = document.getElementById('notifications-count');
    if (notificationsCount != null) {
        notificationsCount.innerHTML = ++notificationsCount.innerHTML;
    } else {
        var element = document.createElement("span");
        element.id = 'notifications-count';
        element.innerHTML = 1;
        document.getElementsByClassName('header__notification-btn')[0].appendChild(element);
    }
    var list = document.getElementsByClassName('notifications-list')[0].children;
    var id = null;

    if (list.length > 1) {
        if (list.length > 5) {
            var child = list[list.length - 2];
            id = child.id.split('-')[2];
            child.parentNode.removeChild(child);
        } else if (list.length <= 4) {
            var child = list[list.length - 1];
            id = child.id.split('-')[2];
        }
        //else {
        //    var child = list[4];
        //    id = child.id.split('-')[2];
        //    child.parentNode.removeChild(child);
        //    alert("g");
        //}
    }


    var newNotification = document.createElement('a');
    newNotification.classList.add('dropdown-notification-bckg-color');
    newNotification.classList.add('notifications-dropdown');
    newNotification.id = `notifications-dropdown-${id}`;
    newNotification.href = `/topic/${topicId}?replyId=${replyId}`;
    newNotification.style.display = 'block';
    newNotification.style.paddingLeft = '20px';
    newNotification.style.marginLeft = '-15px';
    newNotification.style.paddingRight = '20px';
    newNotification.style.marginRight = '-15px';
    photoPath = photoPath.split('~')[1];

    newNotification.innerHTML = `<input id="notification-dropdown-topic-${id}" name="id" value="${topicId}" type="hidden" />
                                      <input id="notification-dropdown-reply-${id}" name="replyId" value="${replyId}" type="hidden" />

                                      <p style="display:flex;position:relative;justify-content: center;align-items: center;margin-top:-0.5rem;margin-bottom:-0.5rem">${dateCreated}</p>
                                      <div style="display:flex;margin-left:-10px">
                                          <img class="profile-img-tiny" src=${photoPath} />
                                          <p style="margin-left:5px;padding-top:0.5rem; max-width:200px; font-weight:800; display:flex;overflow:hidden;overflow-wrap: break-word;position:relative;max-height:1.7rem;word-break: break-word;">${username}</p>
                                      </div>
                                      <p style="display:flex;position:relative;justify-content: center;align-items: center;padding-top:-0.5rem">replied to</p>
                                      <p style="max-width:250px;font-weight:800;display:flex;overflow:hidden;overflow-wrap: break-word;position:relative;max-height:1.5rem;word-break: break-word;margin-bottom:-0.5rem">${topicTitle}</p>
                                  `;
    if (list.length == 1) {
        var noNotifications = document.getElementById('no-notifications');
        if (noNotifications == null) {
            $(document.getElementsByClassName('notifications-list')[0]).prepend(newNotification);
        } else {
            noNotifications.innerHTML = "";
            $(document.getElementsByClassName('notifications-list')[0]).append(newNotification);
        }
    } else {
        $(document.getElementsByClassName('notifications-list')[0]).prepend(newNotification);
    }
    if (list.length > 5) {
        var allNotifications = document.getElementById('view-all-notifications');
        if (allNotifications == null) {
            allNotifications = document.createElement('span');
            allNotifications.id = 'view-all-notifications';
            allNotifications.innerHTML = '<a href="/all-notifications">view all notifications...</a>';
            $(document.getElementsByClassName('notifications-list')[0]).append(allNotifications);
        }
    }
});

$(document).on('click', '.notifications-dropdown', function (e) {
    var id = jQuery(this).attr("id").split('-')[2];
    $.ajax({
        url: '/set-scroll-to-reply-cookie',
        type: 'GET',
        data: { topicId: $('input#notification-dropdown-topic-' + id).val(), replyId: $('input#notification-dropdown-reply-' + id).val() },
        async: false,
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {

        },
        error: function (result) {
        }
    });
});

$(document).on('click', '.fa-bell', function (e) {
    $.ajax({
        url: '/set-notifications-seen',
        type: 'GET',
        async: false,
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            if (result) {
                setTimeout(function () {
                    var notificationsCount = document.getElementById('notifications-count');
                    if (notificationsCount != null) {
                        notificationsCount.outerHTML = "";
                    }
                    $('.notifications-list').children().removeClass('dropdown-notification-bckg-color');
                }, 1200);
            }
        },
        error: function (result) {
        }
    });
});

