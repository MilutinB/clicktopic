﻿
//Check if username is valid
$(document).ready(function () {
    var isUsernameValid = false;

    $('.change-username').on('keydown paste blur cut', function (e) {

        setTimeout(function () {

            if (e.target.id == 'my-profile-change-username') {

                if ($('#my-profile-change-username').valid()) {

                    if ($('#my-profile-change-username-hidden').val() != $('#my-profile-change-username').val()) {

                        $.ajax({
                            url: '/check-username',
                            type: 'GET',
                            dataType: 'json',
                            async: false,
                            data: { username: $('#my-profile-change-username').val() },
                            success: function (result) {

                                if (result == true) {
                                    isUsernameValid = true;
                                    $('#my-profile-username-error-message').empty();
                                } else {
                                    isUsernameValid = false;
                                    $('#my-profile-username-error-message').html('Username already taken.');
                                }
                            },
                            error: function (error) {
                                window.location.href = '../';
                            }
                        });
                    } else {
                        isUsernameValid = false;
                    }

                } else {
                    isUsernameValid = false;
                    $('#my-profile-username-error-message').empty();
                }

            }

            if (isUsernameValid) {
                $('.change-username-submit').removeAttr('disabled');

            } else {
                $('.change-username-submit').attr('disabled', 'disabled');
            }
        }, 50);
    });
});

//Submit username
$('.change-username-submit').click(function (e) {
    var data = {
        username: $('#my-profile-change-username-hidden').val(),
        newUsername: $('#my-profile-change-username').val()
    }
    
    var jsonData = JSON.stringify(data);

    $.ajax({
        url: '/my-profile-change-username',
        type: 'POST',
        dataType: 'json',
        data: jsonData,
        async: false,
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            var resultData = JSON.parse(result);

            if (resultData.Succedded) {
                window.location.href = '../';
            } else {
                $('#my-profile-username-error-message').empty();
                $('#my-profile-username-error-message').html(resultData.Message);
            }
        },
        error: function () {
            window.location.href = '../';
        }
    });
    e.preventDefault();
});

//Check if email is valid
$(document).ready(function () {
    var isEmailValid = false;
    var oldEmail = $('#my-profile-change-email').val();

    $('.change-email').on('keydown paste blur cut', function (e) {

        setTimeout(function () {

            if (e.target.id == 'my-profile-change-email') {

                if ($('#my-profile-change-email').valid()) {

                    if (oldEmail != $('#my-profile-change-email').val()) {

                        $.ajax({
                            url: '/check-email',
                            type: 'GET',
                            dataType: 'json',
                            async: false,
                            data: { email: $('#my-profile-change-email').val() },
                            success: function (result) {

                                if (result == true) {
                                    isEmailValid = true;
                                    $('#my-profile-email-error-message').empty();
                                } else {
                                    isEmailValid = false;
                                    $('#my-profile-email-error-message').html('Email already in use.');
                                }
                            },
                            error: function () {
                                window.location.reload();
                            }
                        });
                    } else {
                        isEmailValid = false;
                    }

                } else {
                    isEmailValid = false;
                    $('#my-profile-email-error-message').empty();
                }

                if (isEmailValid) {
                    $('.change-email-submit').removeAttr('disabled');

                } else {
                    $('.change-email-submit').attr('disabled', 'disabled');
                }
            }
        }, 50);
    });
});

//Submit email
$('.change-email-submit').click(function (e) {
    var data = {
        username: $('#my-profile-change-email-hidden').val(),
        email: $('#my-profile-change-email').val()
    }
    
    var jsonData = JSON.stringify(data);

    $.ajax({
        url: '/my-profile-change-email',
        type: 'POST',
        dataType: 'json',
        data: jsonData,
        async: false,
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            var resultData = JSON.parse(result);

            if (resultData.Succedded) {
                window.location.href = '../';
            } else {
                $('#my-profile-email-error-message').empty();
                $('#my-profile-email-error-message').html(resultData.Message);
            }
        },
        error: function () {
            window.location.href = '../';
        }
    });
    e.preventDefault();
});

//Check if password is valid
$(document).ready(function () {
    var isCurrentValid = false;
    var isNewValid = false;
    var isConfirmValid = false;
    var isExternal = JSON.parse($('#isExternal').val().toLowerCase());

    $('.change-password').on('keydown paste blur cut', function (e) {
        setTimeout(function () {

            if (e.target.id == 'my-profile-change-pass-current') {

                if ($('#my-profile-change-pass-current').valid()) {
                    isCurrentValid = true;

                } else {
                    isCurrentValid = false;
                    //$('#my-profile-email-error-message').empty();
                }
            } else if (e.target.id == 'my-profile-change-pass-new') {

                if ($('#my-profile-change-pass-new').valid()) {

                    if ($('#my-profile-change-pass-new').val() == $('#my-profile-change-pass-confirm').val()) {
                        isNewValid = true;
                        isConfirmValid = true;
                        $('#my-profile-confirm-error-message').empty();
                    } else {
                        $('#my-profile-change-pass-confirm').valid();
                        isNewValid = false;
                    }

                } else {
                    isNewValid = false;
                }
            } else if (e.target.id == 'my-profile-change-pass-confirm') {

                if ($('#my-profile-change-pass-new').valid()) {

                    if ($('#my-profile-change-pass-new').val() == $('#my-profile-change-pass-confirm').val()) {
                        isNewValid = true;
                        isConfirmValid = true;
                        $('#my-profile-confirm-error-message').empty();
                    } else {
                        isConfirmValid = false;
                        $('#my-profile-change-pass-confirm').valid();
                    }

                } else {
                    isConfirmValid = false;
                }
            }

            if (isExternal) {
                if (isNewValid && isConfirmValid) {
                    $('.change-password-submit').removeAttr('disabled');

                } else {
                    $('.change-password-submit').attr('disabled', 'disabled');
                }
            } else {

                if (isCurrentValid && isNewValid && isConfirmValid) {
                    $('.change-password-submit').removeAttr('disabled');

                } else {
                    $('.change-password-submit').attr('disabled', 'disabled');
                }
            }
        }, 50);
    });
});

//Submit password
$('.change-password-submit').click(function (e) {
    var data = {
        username: $('#my-profile-change-pass-hidden').val(),
        currentPassword: $('#my-profile-change-pass-current').val(),
        newPassword: $('#my-profile-change-pass-new').val(),
        confirmPassword: $('#my-profile-change-pass-confirm').val(),
        isExternal: $('#isExternal').val()
    }
    var isExternal = JSON.parse($('#isExternal').val().toLowerCase());

    if (isExternal) {
        data.currentPassword = 'Dummy data';
    }
    
    var jsonData = JSON.stringify(data);

    $.ajax({
        url: '/my-profile-change-password',
        type: 'POST',
        dataType: 'json',
        data: jsonData,
        async: false,
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            var resultData = JSON.parse(result);

            if (resultData.Succedded) {
                window.location.href = '../';
            } else {
                $('#my-profile-password-error-message').empty();
                $('#my-profile-password-error-message').html(resultData.Message);
            }
        },
        error: function () {
            window.location.href = '../';
        }
    });
    e.preventDefault();
});

//Submit delete profile
$('.delete-profile-submit').click(function (e) {
    var data = {
        username: $('#my-profile-delete-profile-hidden').val()
    }

    var jsonData = JSON.stringify(data);

    $.ajax({
        url: '/my-profile-delete',
        type: 'POST',
        dataType: 'json',
        data: jsonData,
        async: false,
        contentType: 'application/json; charset=utf-8',
        beforeSend: function (request) {
            request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
        },
        success: function (result) {
            var resultData = JSON.parse(result);
            if (resultData.Succedded) {
                window.location.href = '../';
            } else {
                $('#my-profile-delete-error-message').empty();
                $('#my-profile-delete-error-message').html(resultData.Message);
            }
        },
        error: function () {
            window.location.href = '../';
        }
    });
    e.preventDefault();
});

//Submit new profile photo
$('.change-photo-submit').click(function (e) {

    var input = document.getElementById('upload-1');
    var file = input.files[0];
    var reader = new FileReader;
    reader.readAsDataURL(file);
    reader.onload = function (e) {
        console.log(reader.result);

        var data = {
            username: $('#my-profile-change-photo-hidden').val(),
            base64: reader.result
        }

        var jsonData = JSON.stringify(data);

        $.ajax({
            url: '/my-profile-new-photo',
            type: 'POST',
            data: jsonData,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function (request) {
                request.setRequestHeader("RequestVerificationToken", $("[name='__RequestVerificationToken']").val());
            },
            success: function (result) {
                var resultData = JSON.parse(result);
                if (resultData.Succedded) {
                    window.location.href = '../';
                } else {
                    $('#my-profile-photo-error-message').html(resultData.Message);
                }
            },
            error: function () {
            }
        }); 
    };
    e.preventDefault();
});

//Change uploaded profile picture if valid
function readURL(input) {
    if (input.files && input.files[0]) {
        const validImageTypes = ['image/gif', 'image/jpeg', 'image/jpg', 'image/png'];
        if (validImageTypes.includes(input.files[0].type)) {

            var reader = new FileReader();
            reader.onload = function (e) {
                var img = new Image();
                img.src = e.target.result;
                img.onload = function () {
                    if (this.width > 0 && this.height > 0) {
                        $('.change-photo-submit').removeAttr('disabled');
                        $('#old-img').attr('src', e.target.result);
                    }
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
        $('.change-photo-submit').attr('disabled', 'disabled');
    }
}

jQuery(function ($) {
    $("#upload-1").change(function () {
        readURL(this);
    });
})

//jQuery(function ($) {
//    $('input[type="file"]').change(function () {
//        if ($(this).val()) {
//            error = false;
//            var filename = $(this).val();

//            $(this).closest('.file-upload').find('.file-name').html(filename);

//            if (error) {
//                parent.addClass('error').prepend.after('<div class="alert alert-error">' + error + '</div>');
//            }
//        }
//    })
//})
