﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using DataAccess.Data;
using DataAccess.Identity;
using DataAccess.Data.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net;
using DataAccess.Services;
using DataAccess.Services.ViewModelServices;
using ApplicationCore.Services.ViewModelServices;
using ApplicationCore.Services;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.CookiePolicy;
using DataAccess.Hubs;
using Microsoft.AspNetCore.HttpOverrides;

namespace Presentation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddHttpsRedirection(options => options.HttpsPort = 44327);
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.HttpOnly = HttpOnlyPolicy.None;
            });

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequiredLength = 8;
                options.Password.RequireUppercase = false;
                options.Password.RequireDigit = true;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireNonAlphanumeric = false;
            });

            services.Configure<SecurityStampValidatorOptions>(options =>
            {
                options.ValidationInterval = TimeSpan.FromMinutes(30);
            });

            services.AddDbContextPool<AppIdentityDbContext>(options =>
                options.UseNpgsql(DbUrlParser.Parse("DATABASE_URL"))); 
            services.AddDbContextPool<AppDbContext>(options => 
                options.UseNpgsql(DbUrlParser.Parse("HEROKU_POSTGRESQL_GREEN_URL")));

            services.AddIdentity<ApplicationUser, IdentityRole<int>>(options => 
            {
                options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzšđžčćABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789ŠĐŽČĆ-_.+/";
            })
                .AddEntityFrameworkStores<AppIdentityDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options => 
            {
                options.LoginPath = "/";
                options.SlidingExpiration = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
            });

            services.AddScoped<ITopicRepository, TopicRepository>();
            services.AddScoped<IReplyRepository, ReplyRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IBookmarkRepository, BookmarkRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<IEnabledNotificationRepository, EnabledNotificationRepository>();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IBookmarkService, BookmarkService> ();
            services.AddScoped<ICommonService, CommonService>();
            services.AddScoped<ITopicService, TopicService>();
            services.AddScoped<IEventSchedulerService, EventSchedulerService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<INotificationsSendService, NotificationsSendService>();

            services.AddScoped<INotificationViewModelService, NotificationViewModelService> ();
            services.AddScoped<ICommonViewModelService, CommonViewModelService>();
            services.AddScoped<IUserViewModelService, UserViewModelService>();
            services.AddScoped<ITopicViewModelService, TopicViewModelService>();

            services.AddHttpContextAccessor();

            services.AddAuthentication()
                .AddGoogle(options => 
                {
                    options.ClientId = "xxxxx";
                    options.ClientSecret = "xxxxx";
                })
                .AddFacebook(options => 
                {
                    options.AppId = "xxxxx";
                    options.AppSecret = "xxxxx";
                })
                .Services.ConfigureApplicationCookie(options => 
                {
                    
                });
            services.Configure<FormOptions>(options =>
            {
                options.ValueLengthLimit = int.MaxValue;
                options.MultipartBodyLengthLimit = int.MaxValue;
                options.MultipartHeadersLengthLimit = int.MaxValue;
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                options.KnownNetworks.Clear();
                options.KnownProxies.Clear();
                options.ForwardLimit = 2;
            });
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseForwardedHeaders();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseStatusCodePagesWithRedirects("/not-found");
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCookiePolicy();
            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseSignalR(routes =>
            {
                routes.MapHub<NotificationHub>("/send-notifications");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
