﻿using ApplicationCore.ViewModels.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.Helpers;
using ApplicationCore.ViewModels.Common;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Authentication;
using Newtonsoft.Json;

namespace Presentation.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService userService;
        private readonly ICommonViewModelService commonViewModelService;
        private readonly IUserViewModelService userViewModelService;
        private readonly ICommonService commonService;

        public UserController(IUserService userService,
                              ICommonViewModelService commonViewModelService,
                              IUserViewModelService userViewModelService,
                              ICommonService commonService)
        {
            this.userService = userService;
            this.commonViewModelService = commonViewModelService;
            this.userViewModelService = userViewModelService;
            this.commonService = commonService;
        }

        [AllowAnonymous]
        [HttpPost("login-user")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([FromBody]UserLoginViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await userService.LoginAsync(viewModel);
                return Json(result.Messages);
            }
            return Json("Wrong username or password.");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await userService.LogoutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Signup([FromBody]UserSignupViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await userService.SignupAsync(viewModel);
                return Json(result.Messages);
            }
            return Json(commonService.GetJsonModelStateError(ModelState));
        }

        [AllowAnonymous]
        [HttpGet("confirm-user-email", Name = "confirm-user-email")]
        public async Task<IActionResult> ConfirmEmail(string username, bool rememberMe, string token)
        {
            if (username == null || token == null)
            {
                ViewBag.ErrorTitle = "Error";
                ViewBag.ErrorMessage = "Confirmation failed. Please try again.";
                return View("Error");
            }

            Result result = await userService.ConfirmedSignupAsync(username, rememberMe, token);

            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ErrorViewModel viewModel = commonViewModelService.GetErrorViewModel(result);
                return View("Error", viewModel);
            }
        }

        [AcceptVerbs("Get")]
        [AllowAnonymous]
        [Route("check-username")]
        public async Task<IActionResult> IsUsernameTaken(string username)
        {
            if (username == null)
            {
                return Json(false);
            }
            bool taken = await userService.IsUsernameTakenAsync(username);

            if (taken)
            {
                return Json(false);
            }
            else
            {
                return Json(true);
            }
        }

        [AcceptVerbs("Get")]
        [AllowAnonymous]
        [Route("check-email")]
        public async Task<IActionResult> IsEmailTaken(string email)
        {
            if (email == null)
            {
                return Json(false);
            }
            bool taken = await userService.IsEmailTakenAsync(email);

            if (taken)
            {
                return Json(false);
            }
            else
            {
                return Json(true);
            }
        }

        [AllowAnonymous]
        [HttpGet("reset-password")]
        public IActionResult PasswordReset()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet("reset-password-email")]
        public async Task<IActionResult> SendResetPasswordEmail(string email)
        {
            await userService.SendResetVerificationCodeAsync(email);

            return Json(true);
        }

        [AllowAnonymous]
        [HttpGet("reset-password-code")]
        public async Task<IActionResult> ConfirmResetPasswordVerificationCode(int code)
        {
            await userService.ConfirmResetVerificationCodeAsync(code);

            return Json(true);
        }

        [HttpGet("my-profile/{username?}")]
        [Authorize(Roles = "User, Admin")]
        public async Task<IActionResult> MyProfile(string username)
        {
            UserProfileViewModel viewModel = await userViewModelService.GetUserEditProfileViewModelAsync(username);

            if (viewModel == null)
            {
                return RedirectToAction("PageNotFound", "Home");
            }
            bool userHasPermission = userService.UserHasPermission(username);

            if (userHasPermission)
            {
                return View(viewModel);
            }
            return RedirectToAction("AccessDenied", "Home");
        }

        [HttpPost("external-login")]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            object properties = userService.GetExternalAuthenticationProperties(provider, returnUrl);
            return new ChallengeResult(provider, (AuthenticationProperties)properties);
        }

        [AllowAnonymous]
        [HttpGet("external-login-callback")]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                ViewBag.ErrorTitle = "Error.";
                ViewBag.ErrorMessage = $"Error from external provider: {remoteError}";
                return View("Error");
            }

            Result result = await userService.ExternalLoginOrCreateUserAsync(returnUrl);

            if (!result.Succeeded)
            {
                ViewBag.ErrorTitle = "Error.";
                ViewBag.ErrorMessage = result.Messages[0];
                return View("Error");
            }
            return LocalRedirect(result.ReturnUrl);
        }

        [HttpPost("my-profile-change-username")]
        [Authorize(Roles = "User, Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditUsername([FromBody]UserChangeUsernameViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await userService.EditUsernameAsync(viewModel);
                return Json(result.Messages);
            }
            return Json(commonService.GetJsonModelStateError(ModelState));
        }

        [HttpPost("my-profile-change-email")]
        [Authorize(Roles = "User, Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditEmail([FromBody]UserChangeEmailViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await userService.EditEmailAsync(viewModel);
                return Json(result.Messages);
            }
            return Json(commonService.GetJsonModelStateError(ModelState));
        }

        [HttpPost("my-profile-change-password")]
        [Authorize(Roles = "User, Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPassword([FromBody]UserChangePasswordViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await userService.EditPasswordAsync(viewModel);
                return Json(result.Messages);
            }
            return Json(commonService.GetJsonModelStateError(ModelState));
        }

        [HttpPost("my-profile-delete")]
        [Authorize(Roles = "User, Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteProfile([FromBody]UserDeleteProfileViewModel viewModel)
        {
            if (viewModel != null)
            {
                Result result = await userService.DeleteProfileAsync(viewModel);
                return Json(result.Messages);
            }
            return Json(commonService.GetJsonError("Error occured. Please try again."));
        }

        [HttpPost("my-profile-new-photo")]
        [Authorize(Roles = "User, Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadNewPhoto([FromBody]UserChangePhotoViewModel viewModel)
        {
            if (viewModel != null && viewModel.Base64.Contains("image"))
            {
                Result result = await userService.UploadProfilePhotoAsync(viewModel);
                return Json(result.Messages);
            }
            return Json(commonService.GetJsonError("Error occured. Please try again."));
        }

        [HttpGet("all-users")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AllUsers()
        {
            return View(await userService.GetAllUsersAsync());
        }

        [HttpPost("delete-user")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            await userService.DeleteUserAsync(id);
            return RedirectToAction("Index", "Home");
        }
    }
}
