﻿using ApplicationCore.Helpers;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels.Topic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Presentation.Controllers
{
    [Authorize]
    public class TopicController : Controller
    {
        private readonly ITopicViewModelService topicViewModelService;
        private readonly ITopicService topicService;
        private readonly ICommonService commonService;
        private readonly IBookmarkService bookmarkService;
        private readonly INotificationRepository notificationRepository;

        public TopicController(ITopicViewModelService topicViewModelService,
                               ITopicService topicService,
                               ICommonService commonService,
                               IBookmarkService bookmarkService,
                               INotificationRepository notificationRepository)
        {
            this.topicViewModelService = topicViewModelService;
            this.topicService = topicService;
            this.commonService = commonService;
            this.bookmarkService = bookmarkService;
            this.notificationRepository = notificationRepository;
        }

        [HttpGet("create-new-topic")]
        public async Task<IActionResult> CreateNewTopic()
        {
            TopicCreateViewModel viewModel = await topicViewModelService.GetCreateViewModelAsync();
            return View(viewModel);
        }

        [HttpPost("create-new-topic")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateNewTopic(TopicCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await topicService.CreateNewTopicAsync(viewModel);

                if (result.Succeeded)
                {
                    return RedirectToAction("ReadTopic", new { Id = result.Messages[0] });
                }
                ModelState.AddModelError("", result.Messages[0]);
            }
            return View(viewModel);
        }

        [AllowAnonymous]
        [HttpGet("topic/{id?}")]
        public async Task<IActionResult> ReadTopic(int? id)
        {
            if (id == null)
            {
                return View("PageNotFound");
            }
            TopicViewModel viewModel = await topicViewModelService.GetTopicViewModelAsync(id.Value);

            if (viewModel == null)
            {
                return View("PageNotFound");
            }
            await topicService.IncreaseTopicViewCountAsync(id.Value);
            return View(viewModel);
        }

        [HttpPost("post-topic-reply")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostReplyToTopic(TopicViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await topicService.ReplyToTopicAsync(viewModel.TopicReplyViewModel);

                if (result.Succeeded)
                {
                    return RedirectToAction("ReadTopic", new { Id = result.Messages[0]  });
                }
                ModelState.AddModelError("", result.Messages[0]);
            }
            return RedirectToAction("ReadTopic", new { Id = viewModel.TopicReplyViewModel.TopicId });
        }

        [HttpPost("post-comment-reply")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostReplyToComment(TopicViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await topicService.ReplyToCommentAsync(viewModel.TopicReplyViewModel);

                if (result.Succeeded)
                {
                    return RedirectToAction("ReadTopic", new { Id = result.Messages[0] });
                }
                ModelState.AddModelError("", result.Messages[0]);
            }
            return RedirectToAction("ReadTopic", new { Id = viewModel.TopicReplyViewModel.TopicId });
        }

        [HttpPost("delete-reply")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteReply(TopicViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await topicService.DeleteReplyAsync(viewModel.TopicDeleteViewModel);

                if (result.Succeeded)
                {
                    return RedirectToAction("ReadTopic", new { Id = result.Messages[0] });
                }
                ModelState.AddModelError("", result.Messages[0]);
            }
            return RedirectToAction("ReadTopic", new { Id = viewModel.TopicDeleteViewModel.TopicId });
        }

        [HttpPost("delete-topic")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteTopic(TopicViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                Result result = await topicService.DeleteTopicAsync(viewModel.TopicDeleteViewModel);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", result.Messages[0]);
            }
            return RedirectToAction("ReadTopic", new { Id = viewModel.TopicDeleteViewModel.TopicId });
        }

        [HttpGet("toggle-topic-bookmarks/{id?}")]
        public async Task<IActionResult> ToggleTopicBookmarks(int? id)
        {
            if (id == null)
            {
                return Json(false);
            }
            Result result = await bookmarkService.ToggleTopicBookmarksAsync(id.Value);
            if (result.Succeeded)
            {
                return Json(true);
            }
            return Json(false);
        }

        [AllowAnonymous]
        [HttpGet("topics/{username?}")]
        public async Task<IActionResult> Topics(string username)
        {
            MyTopicsAggregateViewModel viewModel = await topicViewModelService.GetUserTopics(username);

            if (viewModel.TopicIndexViewModels == null)
            {
                return View("PageNotFound");
            }

            return View(viewModel);
        }
    }
}
