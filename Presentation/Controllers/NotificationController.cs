﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Helpers;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controllers
{
    [Authorize]
    public class NotificationController : Controller
    {
        private readonly INotificationViewModelService notificationViewModelService;
        private readonly ICommonService commonService;
        private readonly INotificationService notificationService;

        public NotificationController(INotificationViewModelService notificationViewModelService, 
                                      ICommonService commonService, 
                                      INotificationService notificationService)
        {
            this.notificationViewModelService = notificationViewModelService;
            this.commonService = commonService;
            this.notificationService = notificationService;
        }

        [HttpGet("all-notifications")]
        public async Task<IActionResult> AllNotifications()
        {
            return View(await notificationViewModelService.GetUserNotificationsAsync());
        }

        [HttpGet("set-scroll-to-reply-cookie")]
        public IActionResult SetScrollToReplyCookie(int topicId, int replyId)
        {
            commonService.SetScrollToReplyCookie(topicId, replyId);
            return Json(true);
        }

        [HttpGet("toggle-topic-notifications/{id?}")]
        public async Task<IActionResult> ToggleTopicNotifications(int? id)
        {
            if (id == null)
            {
                return Json(false);
            }
            Result result = await notificationService.ToggleTopicEnabledNotificationsAsync(id.Value);
            if (result.Succeeded)
            {
                return Json(true);
            }
            return Json(false);
        }

        [HttpGet("set-notifications-seen")]
        public async Task<IActionResult> SetNotificationsSeen()
        {
            Result result = await notificationService.SetNotificationsSeen();
            if (result.Succeeded)
            {
                return Json(true);
            }
            return Json(false);
        }
    }
}