﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels.Topic;
using ApplicationCore.ViewModels.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserViewModelService userViewModelService;
        private readonly ITopicViewModelService topicViewModelService;
        private readonly ICommonService commonService;

        public HomeController(IUserViewModelService userViewModelService, 
                              ITopicViewModelService topicViewModelService,
                              ICommonService commonService)
        {
            this.userViewModelService = userViewModelService;
            this.topicViewModelService = topicViewModelService;
            this.commonService = commonService;
        }

        [HttpGet("/")]
        [HttpGet("/home")]
        [HttpGet("/home/index")]
        public async Task<IActionResult> Index(string returnUrl, string category, string order, int? page, string search)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(await topicViewModelService.GetTopicIndexViewModels(page ?? 1, category, order, search));
        }

        [HttpGet("about")]
        public IActionResult About()
        {
            return View();
        }

        [HttpGet("not-found")]
        public IActionResult PageNotFound()
        {
            return View();
        }

        [HttpGet("access-denied")]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet("welcome")]
        public IActionResult Welcome()
        {
            return View();
        }

        [HttpGet("set-welcome-cookie")]
        public IActionResult SetWelcomeCookie()
        {
            commonService.SetWelcomeCookie();
            return Json(true);
        }

        [HttpGet("error")]
        public IActionResult Error()
        {
            ViewBag.ErrorMessage = "Something went wrong with processing your request.";
            return View();
        }
    }
}
