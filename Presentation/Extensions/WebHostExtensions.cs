﻿using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Presentation.Extensions
{
    public static class WebHostExtensions
    {
        public static IWebHost StartEventScheduler(this IWebHost webHost)
        {
            IServiceScopeFactory serviceScopeFactory = (IServiceScopeFactory)webHost.Services.GetService(typeof(IServiceScopeFactory));

            using (IServiceScope scope = serviceScopeFactory.CreateScope())
            {
                IServiceProvider services = scope.ServiceProvider;
                IEventSchedulerService schedulerService = services.GetRequiredService<IEventSchedulerService>();

                schedulerService.StartScheduler();
            }

            return webHost;
        }
    }
}
