﻿using ApplicationCore.Entities;
using DataAccess.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Bookmark>().ToTable("Bookmark");
            modelBuilder.Entity<Category>().ToTable("Category");
            modelBuilder.Entity<Reply>().ToTable("Reply");
            modelBuilder.Entity<Topic>().ToTable("Topic");
            modelBuilder.Entity<Notification>().ToTable("Notification");
            modelBuilder.Entity<EnabledNotification>().ToTable("EnabledNotification");

            modelBuilder.SeedCategories();
        }
    }
}
