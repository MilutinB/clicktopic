﻿using ApplicationCore.Interfaces;
using DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Data.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly AppDbContext context;

        public BaseRepository(AppDbContext context)
        {
            this.context = context;
        }

        public async Task AddAsync(TEntity entity)
        {
            await context.Set<TEntity>().AddAsync(entity);
        }

        public async Task<TEntity> GetByIdAsync(object id)
        {
            return await context.Set<TEntity>().FindAsync(id);
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            return await context.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions).Where(predicate).FirstOrDefaultAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions).ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions).Where(predicate).ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> orderBy, bool orderDescending)
        {
            if (orderDescending)
            {
                return await context.Set<TEntity>().Where(predicate).OrderByDescending(orderBy).ToListAsync();
            }
            else
            {
                return await context.Set<TEntity>().Where(predicate).OrderBy(orderBy).ToListAsync();
            }
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> orderBy, bool orderDescending, params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            if (orderDescending)
            {
                return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions).Where(predicate).OrderByDescending(orderBy).ToListAsync();
            }
            else
            {
                return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions).Where(predicate).OrderBy(orderBy).ToListAsync();
            }
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> orderBy,
            bool orderDescending,
            int skip,
            int take,
            params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            if (orderDescending)
            {
                return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions)
                    .Where(predicate)
                    .OrderByDescending(orderBy)
                    .Skip(skip)
                    .Take(take)
                    .ToListAsync();
            }
            else
            {
                return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions)
                    .Where(predicate)
                    .OrderBy(orderBy)
                    .Skip(skip)
                    .Take(take)
                    .ToListAsync();
            }
        }

        public async Task<IReadOnlyList<TEntity>> GetAsync(Expression<Func<TEntity, object>> orderBy, 
            bool orderDescending,
            int skip,
            int take,
            params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            if (orderDescending)
            {
                return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions)
                    .OrderByDescending(orderBy)
                    .Skip(skip)
                    .Take(take)
                    .ToListAsync();
            }
            else
            {
                return await AddIncludesToQuery(context.Set<TEntity>(), includeExpressions)
                    .OrderBy(orderBy)
                    .Skip(skip)
                    .Take(take)
                    .ToListAsync();
            }
        }

        public void Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
        }

        public async Task SaveChangesAsync()
        {
            await context.SaveChangesAsync();
        }

        protected IQueryable<TEntity> AddIncludesToQuery(IQueryable<TEntity> dbSet,
            params Expression<Func<TEntity, object>>[] includeExpressions)
        {
            return includeExpressions.Aggregate(dbSet, (current, next) => current.Include(next));
        }
    }
}
