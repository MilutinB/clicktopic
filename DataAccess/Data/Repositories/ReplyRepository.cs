﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Data.Repositories
{
    public class ReplyRepository : BaseRepository<Reply>, IReplyRepository
    {
        public ReplyRepository(AppDbContext context) : base(context)
        {
        }
    }
}
