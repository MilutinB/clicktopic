﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Data.Repositories
{
    public class EnabledNotificationRepository : BaseRepository<EnabledNotification>, IEnabledNotificationRepository
    {
        public EnabledNotificationRepository(AppDbContext context) : base(context)
        {

        }
    }
}
