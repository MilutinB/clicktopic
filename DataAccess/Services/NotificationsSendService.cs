﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using DataAccess.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace DataAccess.Services
{
    public class NotificationsSendService : INotificationsSendService
    {
        private readonly IHubContext<NotificationHub> hubContext;

        public NotificationsSendService(IHubContext<NotificationHub> hubContext)
        {
            this.hubContext = hubContext;
        }

        public async Task SendNotificationsAsync(List<string> userIds, int topicId, int replyId, string dateCreated, string photoPath, string username, string topicTitle)
        {
            await hubContext.Clients.Users(userIds).SendAsync("ReceiveNotification", topicId, replyId, dateCreated, photoPath, username, topicTitle);
        }
    }
}