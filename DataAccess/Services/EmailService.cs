﻿using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MimeKit;
using MailKit;
using MailKit.Net.Smtp;

namespace DataAccess.Services
{
    public class EmailService : IEmailService
    {
        private string username = "clicktopicapp@gmail.com";
        private string password = "xxxxx";

        public async Task SendConfirmationEmailAsync(string address, string confirmationLink)
        {
            string messageBody = $"Thank you for joining our community.<br><br>" +
                $"Please verify your email by following the link below:<br><br>" +
                $"<a href = {confirmationLink}>{confirmationLink}</a><br><br>" +
                $"Feel free to email us at this address if you have any questions about ClickTopic.<br><br>" +
                $"Happy chatting!";

            MimeMessage message = new MimeMessage();
            message.From.Add(new MailboxAddress("ClickTopic", "clicktopicapp@gmail.com"));
            message.To.Add(new MailboxAddress(address));
            message.Subject = "ClickTopic sign up confirmation";
            message.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = messageBody
            };

            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    if (!client.IsConnected)
                    {
                        await client.ConnectAsync("smtp.gmail.com", 587, false);
                    }
                    if (!client.IsAuthenticated)
                    {
                        await client.AuthenticateAsync(username, password);
                    }
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception)
            {
                //
            }
        }

        public async Task SendPasswordVerificationCodeAsync(string address, int verificationCode)
        {
            string messageBody = $"We have sent you this email in response to your request to reset password on ClickTopic.<br>" +
                $"To reset password enter the verification code below on our reset password page.<br>" +
                $"<h4><bold>{verificationCode}</bold></h4><br>" +
                $"After succefull verification we will send you a new password that can be changed in your profile settings.<br><br>" +
                $"Happy chatting!";

            MimeMessage message = new MimeMessage();
            message.From.Add(new MailboxAddress("ClickTopic", "clicktopicapp@gmail.com"));
            message.To.Add(new MailboxAddress(address));
            message.Subject = "ClickTopic reset password verification code";
            message.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = messageBody
            };

            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    if (!client.IsConnected)
                    {
                        await client.ConnectAsync("smtp.gmail.com", 587, false);
                    }
                    if (!client.IsAuthenticated)
                    {
                        await client.AuthenticateAsync(username, password);
                    }
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception)
            {
                //
            }
        }

        public async Task SendNewPasswordAsync(string address, string newUserPassword)
        {
            string messageBody = $"Your password has been succefully changed.<br>" +
                $"Your new password is:<br>" +
                $"<h4><bold>{newUserPassword}</bold></h4>" +
                $"After succefull log in you can set a new password that can be changed in your profile settings.<br><br>" +
                $"Happy chatting!";

            MimeMessage message = new MimeMessage();
            message.From.Add(new MailboxAddress("ClickTopic", "clicktopicapp@gmail.com"));
            message.To.Add(new MailboxAddress(address));
            message.Subject = "ClickTopic new password";
            message.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = messageBody
            };

            try
            {
                using (SmtpClient client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                    if (!client.IsConnected)
                    {
                        await client.ConnectAsync("smtp.gmail.com", 587, false);
                    }
                    if (!client.IsAuthenticated)
                    {
                        await client.AuthenticateAsync(username, password);
                    }
                    await client.SendAsync(message);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception)
            {
                //
            }
        }
    }
}
