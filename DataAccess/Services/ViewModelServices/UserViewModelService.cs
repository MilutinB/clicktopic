﻿using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels.User;
using DataAccess.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Services.ViewModelServices
{
    public class UserViewModelService : IUserViewModelService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IHttpContextAccessor accessor;

        public UserViewModelService(UserManager<ApplicationUser> userManager, IHttpContextAccessor accessor)
        {
            this.userManager = userManager;
            this.accessor = accessor;
        }

        public UserViewModel GetUserViewModel(string returnUrl)
        {
            return new UserViewModel
            {
                UserLoginViewModel = new UserLoginViewModel
                {
                    ReturnUrl = returnUrl
                },
                UserSignupViewModel = new UserSignupViewModel
                {
                    ReturnUrl = returnUrl
                }
            };
        }

        public async Task<UserProfileViewModel> GetUserEditProfileViewModelAsync(string username)
        {
            UserProfileViewModel viewModel = null;
            ApplicationUser user = null;

            if (username == null)
            {
                user = await userManager.GetUserAsync(accessor.HttpContext.User);
            }
            else
            {
                user = await userManager.FindByNameAsync(username);
            }

            if (user != null)
            {
                viewModel = new UserProfileViewModel();
                viewModel.UserChangeUsernameViewModel = new UserChangeUsernameViewModel
                {
                    NewUsername = user.UserName,
                    Username = user.UserName
                };
                viewModel.UserChangeEmailViewModel = new UserChangeEmailViewModel
                {
                    Username = user.UserName,
                    Email = user.Email
                };
                viewModel.UserChangePasswordViewModel = new UserChangePasswordViewModel
                {
                    Username = user.UserName,
                    IsExternal = user.PasswordHash == null ? true : false
                };
                viewModel.UserDeleteProfileViewModel = new UserDeleteProfileViewModel
                {
                    Username = user.UserName
                };
                viewModel.UserChangePhotoViewModel = new UserChangePhotoViewModel
                {
                    Username = user.UserName,
                    PhotoPath = user.PhotoPath
                };
            }
            return viewModel;
        }
    }
}
