﻿using ApplicationCore.Helpers;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels.User;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using System.Security.Claims;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Authentication;
using System.IO;
using System.Drawing;
using ApplicationCore.Entities;

namespace DataAccess.Identity
{
    public class UserService : IUserService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IEmailService emailService;
        private readonly IHttpContextAccessor accessor;
        private readonly LinkGenerator generator;
        private readonly IServiceScopeFactory scopeFactory;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ITopicRepository topicRepository;
        private readonly IReplyRepository replyRepository;
        private readonly INotificationRepository notificationRepository;
        private readonly IBookmarkRepository bookmarkRepository;
        private readonly IEnabledNotificationRepository enabledNotificationRepository;

        public UserService(UserManager<ApplicationUser> userManager,
                           SignInManager<ApplicationUser> signInManager,
                           IEmailService emailService,
                           IHttpContextAccessor accessor,
                           LinkGenerator generator,
                           IServiceScopeFactory scopeFactory,
                           IHostingEnvironment hostingEnvironment,
                           ITopicRepository topicRepository,
                           IReplyRepository replyRepository,
                           INotificationRepository notificationRepository,
                           IBookmarkRepository bookmarkRepository,
                           IEnabledNotificationRepository enabledNotificationRepository)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.emailService = emailService;
            this.accessor = accessor;
            this.generator = generator;
            this.scopeFactory = scopeFactory;
            this.hostingEnvironment = hostingEnvironment;
            this.topicRepository = topicRepository;
            this.replyRepository = replyRepository;
            this.notificationRepository = notificationRepository;
            this.bookmarkRepository = bookmarkRepository;
            this.enabledNotificationRepository = enabledNotificationRepository;
        }

        public async Task<Result> LoginAsync(UserLoginViewModel viewModel)
        {
            ApplicationUser user = await userManager.FindByNameAsync(viewModel.UserName);

            string failedJson = JsonConvert.SerializeObject(new
            {
                Succedded = false,
                Message = "Wrong username or password."
            });

            if (user == null || !user.EmailConfirmed)
            {
                return new Result(false, new string[] { failedJson });
            }

            Microsoft.AspNetCore.Identity.SignInResult result = await signInManager
                .PasswordSignInAsync(user, viewModel.Password, viewModel.RememberMe, false);

            if (result.Succeeded)
            {
                if (viewModel.ReturnUrl != null)
                {
                    string successJsonWithReturnUrl = JsonConvert.SerializeObject(new
                    {
                        Succedded = true,
                        ReturnUrl = viewModel.ReturnUrl
                    });
                    return new Result(true, new string[] { successJsonWithReturnUrl });
                }
                string successJsonWithoutReturnUrl = JsonConvert.SerializeObject(new
                {
                    Succedded = true,
                });
                return new Result(true, new string[] { successJsonWithoutReturnUrl });
            }

            return new Result(false, new string[] { failedJson });
        }

        public async Task LogoutAsync()
        {
            await signInManager.SignOutAsync();
        }

        public async Task<Result> SignupAsync(UserSignupViewModel viewModel)
        {
            string email = NormalizeEmail(viewModel.Email);
            ApplicationUser user = await userManager.FindByEmailAsync(viewModel.Email);

            if (user == null)
            {
                string photoPath = GenerateDefaultProfileImagePath(viewModel.UserName);
                ApplicationUser newUser = new ApplicationUser
                {
                    UserName = viewModel.UserName,
                    Email = email,
                    PhotoPath = photoPath
                };
                IdentityResult createResult = await userManager.CreateAsync(newUser, viewModel.Password);

                if (createResult.Succeeded)
                {
                    IdentityResult tokenResult = await AddTokenExpirationAsync(newUser);

                    if (tokenResult.Succeeded)
                    {
                        string token = await userManager.GenerateEmailConfirmationTokenAsync(newUser);
                        string confirmationLink = GetConfirmationLink(viewModel.UserName, viewModel.RememberMe, token);
                        await emailService.SendConfirmationEmailAsync(viewModel.Email, confirmationLink);

                        string successJson = JsonConvert.SerializeObject(new
                        {
                            Succedded = true,
                            Message = "Confirmation link has been sent to your email address. Please check your inbox."
                        });
                        return new Result(true, new string[] { successJson });
                    }

                    string failedTokenJson = JsonConvert.SerializeObject(new
                    {
                        Succedded = false,
                        Message = tokenResult.Errors.Select(e => e.Description).First()
                    });
                    return new Result(false, new string[] { failedTokenJson });
                }
                string failedCreateJson = JsonConvert.SerializeObject(new
                {
                    Succedded = false,
                    Message = createResult.Errors.Select(e => e.Description).First()
                });
                return new Result(false, new string[] { failedCreateJson });
            }

            if (user.EmailConfirmed)
            {
                string confirmedJson = JsonConvert.SerializeObject(new
                {
                    Succedded = false,
                    Message = "User already exists."
                });
                return new Result(false, new string[] { confirmedJson });
            }

            string existsJson = JsonConvert.SerializeObject(new
            {
                Succedded = false,
                Message = "Confirmation link has already been sent to your email address."
            });
            return new Result(false, new string[] { existsJson });
        }

        public async Task<Result> ConfirmedSignupAsync(string username, bool rememberMe, string token)
        {
            ApplicationUser user = await userManager.FindByNameAsync(username);

            if (user == null)
            {
                return new Result(false, new string[] { "Sign up failed, please try again. If the problem persists contact site administrator." });
            }

            IdentityResult confirmResult = await userManager.ConfirmEmailAsync(user, token);

            if (!confirmResult.Succeeded)
            {
                return new Result(false, new string[] { "Sign up failed, please try again. If the problem persists contact site administrator." });
            }

            IdentityResult roleResult = await userManager.AddToRoleAsync(user, "User");

            if (!roleResult.Succeeded)
            {
                return new Result(false, new string[] { "Sign up failed, please try again. If the problem persists contact site administrator." });
            }

            user.ConfirmationTokenExpiration = null;
            IdentityResult tokenResult = await userManager.UpdateAsync(user);

            if (!tokenResult.Succeeded)
            {
                return new Result(false, new string[] { "Sign up failed, please try again. If the problem persists contact site administrator." });
            }

            await signInManager.SignInAsync(user, rememberMe);
            return new Result(true, null);
        }

        public async Task<bool> IsUsernameTakenAsync(string username)
        {
            ApplicationUser user = await userManager.FindByNameAsync(username);
            IReadOnlyList<Topic> topics = await topicRepository.GetAsync(t => t.Username == username);

            if (topics.Count > 0)
            {
                return true;
            }
            IReadOnlyList<Reply> replies = await replyRepository.GetAsync(r => r.Username == username);

            if (replies.Count > 0)
            {
                return true;
            }

            if (user?.UserName == accessor.HttpContext.User?.Identity.Name)
            {
                return false;
            }
            return user == null ? false : true;
        }

        public async Task<bool> IsEmailTakenAsync(string email)
        {
            email = NormalizeEmail(email);
            ApplicationUser user = await userManager.FindByEmailAsync(email);

            if (user?.UserName == accessor.HttpContext.User?.Identity.Name)
            {
                return false;
            }
            return user == null ? false : true;
        }

        private string GetConfirmationLink(string username, bool rememberMe, string token)
        {
            return generator.GetUriByRouteValues
                (accessor.HttpContext,
                "confirm-user-email",
                new { username, rememberMe, token },
                accessor.HttpContext.Request.Scheme);
        }

        private string GetRedirectUrlLink(string action, string controller, string returnUrl)
        {
            if (returnUrl == null)
            {
                return generator.GetPathByAction(action, controller);
            }
            return generator.GetUriByAction
                (accessor.HttpContext,
                action,
                controller,
                new { returnUrl },
                accessor.HttpContext.Request.Scheme);
        }

        private async Task<IdentityResult> AddTokenExpirationAsync(ApplicationUser user)
        {
            user.ConfirmationTokenExpiration = DateTime.Now.AddHours(24);
            return await userManager.UpdateAsync(user);
        }

        public async Task DeleteExpiredTokenUsersAsync()
        {
            using (IServiceScope scope = scopeFactory.CreateScope())
            {
                IServiceProvider services = scope.ServiceProvider;
                UserManager<ApplicationUser> scheduledManager = services.GetRequiredService<UserManager<ApplicationUser>>();

                foreach (ApplicationUser user in scheduledManager.Users.ToList())
                {
                    if (user.ConfirmationTokenExpiration != null)
                    {
                        TimeSpan span = DateTime.Now - user.ConfirmationTokenExpiration.Value;

                        if (span.Hours > 24)
                        {
                            await scheduledManager.DeleteAsync(user);
                        }
                    }
                }
            }

        }

        public async Task SendResetVerificationCodeAsync(string email)
        {
            email = NormalizeEmail(email);
            ApplicationUser user = await userManager.FindByEmailAsync(email);

            if (user != null)
            {
                int verificationCode = GeneratePasswordVerificationCode();
                IdentityResult codeResult = await AddUserVerificationCodeAsync(user, verificationCode);

                if (codeResult.Succeeded)
                {
                    await emailService.SendPasswordVerificationCodeAsync(email, verificationCode);
                }
            }
        }

        private int GeneratePasswordVerificationCode()
        {
            Random random = new Random();
            string code = "";

            for (int i = 0; i < 6; i++)
            {
                code += random.Next(0, 10);
            }

            return int.Parse(code);
        }

        private async Task<IdentityResult> AddUserVerificationCodeAsync(ApplicationUser user, int verificationCode)
        {
            user.PasswordRecoveryCode = verificationCode;
            return await userManager.UpdateAsync(user);
        }

        public async Task ConfirmResetVerificationCodeAsync(int code)
        {
            ApplicationUser user = await userManager.Users.Where(u => u.PasswordRecoveryCode == code).FirstOrDefaultAsync();

            if (user != null)
            {
                string newPassword = GeneratePassword();
                string hashedPassword = GenerateNewPasswordHash(user, newPassword);
                user.PasswordHash = hashedPassword;
                user.PasswordRecoveryCode = null;
                IdentityResult result = await userManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    await emailService.SendNewPasswordAsync(user.Email, newPassword);
                }
            }
        }

        private string GeneratePassword()
        {
            string letters = "abcdefghijklmnopqrstuvwxyz";
            string code = "";
            Random random = new Random();
            code += letters[random.Next(0, 26)];
            code += random.Next(0, 10).ToString();
            code += Guid.NewGuid().ToString().Split('-')[0];
            return code;
        }

        private string GenerateNewPasswordHash(ApplicationUser user, string password)
        {
            PasswordHasher<ApplicationUser> hasher = new PasswordHasher<ApplicationUser>();
            return hasher.HashPassword(user, password);
        }

        private string NormalizeEmail(string email)
        {
            string[] splitEmail = email.Split('@');
            StringBuilder builder = new StringBuilder();

            foreach (char c in splitEmail[0])
            {
                if (c != '.')
                {
                    builder.Append(c);
                }
            }
            builder.Append('@');
            builder.Append(splitEmail[1]);

            return builder.ToString();
        }

        public bool UserHasPermission(string username)
        {
            return accessor.HttpContext.User.Identity.Name == username || username == null;
        }

        public async Task<bool> UserIsAdmin(string username)
        {
            ApplicationUser user = await userManager.FindByNameAsync(username);

            if (user == null)
            {
                return false;
            }
            return await userManager.IsInRoleAsync(user, "Admin");
        }

        public async Task<bool> CurrentUserIsAdminAsync()
        {
            ApplicationUser user = await userManager.FindByNameAsync(accessor.HttpContext.User.Identity.Name);

            if (user == null)
            {
                return false;
            }
            return await userManager.IsInRoleAsync(user, "Admin");
        }

        public object GetExternalAuthenticationProperties(string provider, string returnUrl)
        {
            string redirectUrl = GetRedirectUrlLink("ExternalLoginCallback", "User", returnUrl);
            return signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
        }

        public async Task<Result> ExternalLoginOrCreateUserAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? GetRedirectUrlLink("Index", "Home", null);
            ExternalLoginInfo loginInfo = await signInManager.GetExternalLoginInfoAsync();
            string email = loginInfo?.Principal.FindFirstValue(ClaimTypes.Email);

            if (loginInfo == null || email == null)
            {
                return new Result(false, new string[] { $"Error loading external login information. Please try again." });
            }

            Microsoft.AspNetCore.Identity.SignInResult signInResult =
                await signInManager.ExternalLoginSignInAsync(loginInfo.LoginProvider, loginInfo.ProviderKey, false, true);

            if (signInResult.Succeeded)
            {
                return new Result(true, null, returnUrl);
            }

            email = NormalizeEmail(email);
            ApplicationUser user = await userManager.FindByEmailAsync(email);

            if (user == null)
            {
                string username = await GenerateUsernameAsync(email);

                user = new ApplicationUser
                {
                    UserName = username,
                    Email = email,
                    EmailConfirmed = true,
                    PhotoPath = GenerateDefaultProfileImagePath(username)
                };

                IdentityResult createResult = await userManager.CreateAsync(user);

                if (!createResult.Succeeded)
                {
                    return new Result(false, new string[] { $"Error loading external login information. Please try again." });
                }
            }

            IdentityResult addLoginResult = await userManager.AddLoginAsync(user, loginInfo);

            if (!addLoginResult.Succeeded)
            {
                return new Result(false, new string[] { $"Error loading external login information. Please try again." });
            }
            await signInManager.SignInAsync(user, true);

            return new Result(true, null, returnUrl);
        }

        private async Task<string> GenerateUsernameAsync(string email)
        {
            string username = email.Split('@')[0];

            if (username.Length > 20)
            {
                StringBuilder builder = new StringBuilder();

                for (int i = 0; i < 20; i++)
                {
                    builder.Append(username[i]);
                }
                username = builder.ToString();
            }

            ApplicationUser user = await userManager.FindByNameAsync(username);

            if (user == null)
            {
                return username;
            }
            int tries = 0;

            while (tries < 10)
            {
                username = GetRandomUsername();
                user = await userManager.FindByNameAsync(username);

                if (user == null)
                {
                    return username;
                }
                tries++;
            }
            return "user_" + Guid.NewGuid().ToString().Split("-")[0];
        }

        private string GetRandomUsername()
        {
            List<string> verbs = new List<string> { "Advanced", "Balanced", "Amazed", "Blossomed", "Delighted", "Fascinated", "Impressed", "Lovable", 
                "Happy", "Relaxed", "Puffy", "Pretty", "Motivated", "Confident", "Dreamy", "Relaxed", "Witty", "Stellar", "Magical" };
            List<string> nouns = new List<string> { "Toaster", "Squirrel", "Balloon", "Flower", "Cookie", "Egg", "Clock", "Motor", "Lobster",
                "Tomato", "Frog", "Rocket", "Comet", "Duck", "Lamp", "Box", "Cloud", "Girrafe" };

            Random random = new Random();
            string verb = verbs[random.Next(0, 21)]; 
            string noun = nouns[random.Next(0, 21)];
            return verb + "_" + noun;
        }

        public async Task<Result> EditUsernameAsync(UserChangeUsernameViewModel viewModel)
        {
            ApplicationUser user = await userManager.FindByNameAsync(viewModel.NewUsername);

            if (user != null && user.UserName != accessor.HttpContext.User.Identity.Name)
            {
                string nameTakenJson = JsonConvert.SerializeObject(new
                {
                    Succedded = false,
                    Message = "Username already taken."
                });
                return new Result(false, new string[] { nameTakenJson });
            }
            user = await userManager.FindByNameAsync(viewModel.Username);

            if (user != null)
            {
                user.UserName = viewModel.NewUsername;

                if (!user.PhotoPath.Contains("base64"))
                {
                    user.PhotoPath = GenerateDefaultProfileImagePath(viewModel.NewUsername);
                }
                IdentityResult identityResult = await userManager.UpdateAsync(user);

                if (identityResult.Succeeded)
                {
                    await signInManager.RefreshSignInAsync(user);

                    await UpdateTopicsUsernameAsync(viewModel.Username, viewModel.NewUsername);
                    await UpdateRepliesUsernameAsync(viewModel.Username, viewModel.NewUsername);
                    await UpdateNotificationsUsernameAsync(viewModel.Username, viewModel.NewUsername);

                    string successJson = JsonConvert.SerializeObject(new
                    {
                        Succedded = true
                    });
                    return new Result(true, new string[] { successJson });
                }
                string updateFailedJson = JsonConvert.SerializeObject(new
                {
                    Succedded = false,
                    Message = identityResult.Errors.Select(e => e.Description).First()
                });
                return new Result(false, new string[] { updateFailedJson });
            }
            string userNotFoundJson = JsonConvert.SerializeObject(new
            {
                Succedded = false,
                Message = "User profile cannot be found. It may be deleted or corrupted."
            });
            return new Result(false, new string[] { userNotFoundJson });
        }

        public async Task<Result> EditEmailAsync(UserChangeEmailViewModel viewModel)
        {
            string email = NormalizeEmail(viewModel.Email);
            ApplicationUser user = await userManager.FindByNameAsync(viewModel.Username);

            if (user != null)
            {
                user.Email = email;
                IdentityResult identityResult = await userManager.UpdateAsync(user);

                if (identityResult.Succeeded)
                {
                    string successJson = JsonConvert.SerializeObject(new
                    {
                        Succedded = true
                    });
                    return new Result(true, new string[] { successJson });
                }
                string updateFailedJson = JsonConvert.SerializeObject(new
                {
                    Succedded = false,
                    Message = identityResult.Errors.Select(e => e.Description).First()
                });
                return new Result(false, new string[] { updateFailedJson });
            }
            string userNotFoundJson = JsonConvert.SerializeObject(new
            {
                Succedded = false,
                Message = "User profile cannot be found. It may be deleted or corrupted."
            });
            return new Result(false, new string[] { userNotFoundJson });
        }

        public async Task<Result> EditPasswordAsync(UserChangePasswordViewModel viewModel)
        {
            ApplicationUser user = await userManager.FindByNameAsync(viewModel.Username);

            if (user != null)
            {
                if (viewModel.IsExternal)
                {
                    IdentityResult addPasswordResult = await userManager.AddPasswordAsync(user, viewModel.NewPassword);

                    if (addPasswordResult.Succeeded)
                    {
                        await signInManager.RefreshSignInAsync(user);
                        string successJson = JsonConvert.SerializeObject(new
                        {
                            Succedded = true
                        });
                        return new Result(true, new string[] { successJson });
                    }
                    string failedJson = JsonConvert.SerializeObject(new
                    {
                        Succedded = false,
                        Message = addPasswordResult.Errors.Select(e => e.Description).First()
                    });
                    return new Result(false, new string[] { failedJson });
                }

                bool isCorrectPassword = await userManager.CheckPasswordAsync(user, viewModel.CurrentPassword);

                if (isCorrectPassword)
                {
                    IdentityResult changePasswordResult = await userManager
                        .ChangePasswordAsync(user, viewModel.CurrentPassword, viewModel.NewPassword);

                    if (changePasswordResult.Succeeded)
                    {
                        await signInManager.RefreshSignInAsync(user);
                        string successJson = JsonConvert.SerializeObject(new
                        {
                            Succedded = true
                        });
                        return new Result(true, new string[] { successJson });
                    }
                    string failedJson = JsonConvert.SerializeObject(new
                    {
                        Succedded = false,
                        Message = changePasswordResult.Errors.Select(e => e.Description).First()
                    });
                    return new Result(false, new string[] { failedJson });
                }
                string wrongPasswordJson = JsonConvert.SerializeObject(new
                {
                    Succedded = false,
                    Message = "Wrong password."
                });
                return new Result(false, new string[] { wrongPasswordJson });
            }
            string userNotFoundJson = JsonConvert.SerializeObject(new
            {
                Succedded = false,
                Message = "User profile cannot be found. It may be deleted or corrupted."
            });
            return new Result(false, new string[] { userNotFoundJson });
        }

        public async Task<Result> DeleteProfileAsync(UserDeleteProfileViewModel viewModel)
        {
            ApplicationUser user = await userManager.FindByNameAsync(viewModel.Username);

            if (user != null)
            {
                int id = user.Id;
                string username = user.UserName;

                IdentityResult deleteResult = await userManager.DeleteAsync(user);

                if (deleteResult.Succeeded)
                {
                    await signInManager.SignOutAsync();
                    string successJson = JsonConvert.SerializeObject(new
                    {
                        Succedded = true
                    });

                    await DeleteUserBookmarksAndNotificationsAsync(username, id);
                    return new Result(true, new string[] { successJson });
                }
                string failedJson = JsonConvert.SerializeObject(new
                {
                    Succedded = false,
                    Message = deleteResult.Errors.Select(e => e.Description).First()
                });
                return new Result(false, new string[] { failedJson });
            }
            string userNotFoundJson = JsonConvert.SerializeObject(new
            {
                Succedded = false,
                Message = "User profile cannot be found. It may be deleted or corrupted."
            });
            return new Result(false, new string[] { userNotFoundJson });
        }

        public async Task<Result> UploadProfilePhotoAsync(UserChangePhotoViewModel viewModel)
        {
            ApplicationUser user = await userManager.FindByNameAsync(viewModel.Username);

            if (user != null)
            {
                user.PhotoPath = viewModel.Base64;
                IdentityResult identityResult = await userManager.UpdateAsync(user);

                if (identityResult.Succeeded)
                {
                    string successJson2 = JsonConvert.SerializeObject(new
                    {
                        Succedded = true
                    });
                    return new Result(true, new string[] { successJson2 });
                }
                string updateFailedJson = JsonConvert.SerializeObject(new
                {
                    Succedded = false,
                    Message = identityResult.Errors.Select(e => e.Description).First()
                });
                return new Result(false, new string[] { updateFailedJson });
            }
            string failedJson2 = JsonConvert.SerializeObject(new
            {
                Succedded = false,
                Message = "We had issues processing your request. Please try again."
            });
            return new Result(false, new string[] { failedJson2 });
        }

        private string GenerateDefaultProfileImagePath(string username)
        {
            return $"~/pictures/{username[0].ToString().ToUpper()}.svg";
        }

        public async Task<UserInfo> GetUserInfoAsync(string username)
        {
            ApplicationUser user = await userManager.FindByNameAsync(username);
            IReadOnlyList<Topic> topics = null;

            if (user != null)
            {
                topics = await topicRepository.GetAsync(t => t.Username == username);
            }

            return new UserInfo
            {
                PhotoPath = user == null ? GenerateDefaultProfileImagePath(username) : user.PhotoPath,
                TopicCount = topics == null ? 0 : topics.Count,
                Username = username,
                Id = user?.Id
            };
        }

        public async Task<UserInfo> GetUserInfoAsync(int id)
        {
            ApplicationUser user = await userManager.FindByIdAsync(id.ToString());
            IReadOnlyList<Topic> topics = null;

            if (user != null)
            {
                topics = await topicRepository.GetAsync(t => t.Username == user.UserName);
            }

            return new UserInfo
            {
                PhotoPath = user == null ? GenerateDefaultProfileImagePath(user.UserName) : user.PhotoPath,
                TopicCount = topics == null ? 0 : topics.Count,
                Username = user.UserName,
                Id = user?.Id
            };
        }

        public async Task<UserInfo> GetCurrentUserInfoAsync()
        {
            ApplicationUser user = await userManager.FindByNameAsync(accessor.HttpContext.User.Identity.Name);
            IReadOnlyList<Topic> topics = null;

            if (user != null)
            {
                topics = await topicRepository.GetAsync(t => t.Username == user.UserName);
            }

            return new UserInfo
            {
                PhotoPath = user == null ? GenerateDefaultProfileImagePath(accessor.HttpContext.User.Identity.Name) : user.PhotoPath,
                TopicCount = topics == null ? 0 : topics.Count,
                Username = user == null ? accessor.HttpContext.User.Identity.Name : user.UserName,
                Id = user?.Id
            };
        }

        private async Task UpdateTopicsUsernameAsync(string username, string newUsername)
        {
            IReadOnlyList<Topic> topics = await topicRepository.GetAsync(t => t.Username == username);

            if (topics != null || topics.Count > 0)
            {
                foreach (Topic topic in topics)
                {
                    topic.Username = newUsername;
                }
            }
            try
            {
                await topicRepository.SaveChangesAsync();
            }
            catch (Exception)
            {

            }
        }

        private async Task UpdateRepliesUsernameAsync(string username, string newUsername)
        {
            IReadOnlyList<Reply> replies = await replyRepository.GetAsync(r => r.Username == username);

            if (replies != null || replies.Count > 0)
            {
                foreach (Reply reply in replies)
                {
                    reply.Username = newUsername;
                }
            }
            try
            {
                await replyRepository.SaveChangesAsync();
            }
            catch (Exception)
            {
                //
            }
        }

        private async Task UpdateNotificationsUsernameAsync(string username, string newUsername)
        {
            IReadOnlyList<Notification> notifications = await notificationRepository.GetAsync(n => n.Username == username);

            if (notifications != null || notifications.Count > 0)
            {
                foreach (Notification notification in notifications)
                {
                    notification.Username = newUsername;
                }
                try
                {
                    await notificationRepository.SaveChangesAsync();
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        public bool UserIsLoggedIn()
        {
            return accessor.HttpContext.User.Identity.Name != null;
        }

        public async Task<IReadOnlyList<UserInfo>> GetAllUsersAsync()
        {
            List<UserInfo> infos = new List<UserInfo>();
            List<ApplicationUser> users = await userManager.Users.ToListAsync();

            if (users.Count > 0)
            {
                foreach (ApplicationUser user in users)
                {
                    UserInfo info = new UserInfo
                    {
                        Username = user.UserName,
                        Id = user.Id
                    };
                    infos.Add(info);
                }
            }
            return infos;
        }

        public async Task DeleteUserAsync(int id)
        {
            ApplicationUser user = await userManager.FindByIdAsync(id.ToString());

            if (user != null)
            {
                string username = user.UserName;
                string photoPath = user.PhotoPath;

                try
                {
                    IdentityResult updateResult = await userManager.UpdateSecurityStampAsync(user);

                    if (updateResult.Succeeded)
                    {
                        IdentityResult deleteResult = await userManager.DeleteAsync(user);

                        if (deleteResult.Succeeded)
                        {
                            await DeleteUserBookmarksAndNotificationsAsync(username, id, true);
                        }
                    }
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        private async Task DeleteUserBookmarksAndNotificationsAsync(string username, int id, bool isAdminDeleted = false)
        {
            if (isAdminDeleted)
            {
                IReadOnlyList<Notification> notifications = await notificationRepository.GetAsync(t => t.Username == username);

                if (notifications.Count > 0)
                {
                    foreach (Notification notification in notifications)
                    {
                        notificationRepository.Delete(notification);
                    }
                }
            }
            IReadOnlyList<Bookmark> bookmarks = await bookmarkRepository.GetAsync(b => b.UserId == id);
            IReadOnlyList<EnabledNotification> enabledNotifications = await enabledNotificationRepository.GetAsync(e => e.UserId == id);

            if (bookmarks.Count > 0)
            {
                foreach (Bookmark bookmark in bookmarks)
                {
                    bookmarkRepository.Delete(bookmark);
                }
            }
            if (enabledNotifications.Count > 0)
            {
                foreach (EnabledNotification enabledNotification in enabledNotifications)
                {
                    enabledNotificationRepository.Delete(enabledNotification);
                }
            }
            try
            {
                await notificationRepository.SaveChangesAsync();
                await bookmarkRepository.SaveChangesAsync();
                await enabledNotificationRepository.SaveChangesAsync();
            }
            catch (Exception)
            {
                //
            }
        }
    }
}

