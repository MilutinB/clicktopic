﻿using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using FluentScheduler;

namespace DataAccess.Services
{
    public class EventSchedulerService : IEventSchedulerService
    {
        private readonly IUserService userService;

        public EventSchedulerService(IUserService userService)
        {
            this.userService = userService;
        }

        public void StartScheduler()
        {
            Registry registry = new Registry();
            registry.Schedule(async () => await userService.DeleteExpiredTokenUsersAsync()).ToRunEvery(1).Hours();
            JobManager.Initialize(registry);
        }
    }
}
