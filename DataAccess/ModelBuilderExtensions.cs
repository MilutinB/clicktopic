﻿using ApplicationCore.Entities;
using DataAccess.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public static class ModelBuilderExtensions
    {
        public static void SeedIdentityDb(this ModelBuilder modelBuilder)
        {
            string securityStamp1 = Guid.NewGuid().ToString();
            string securityStamp2 = Guid.NewGuid().ToString();

            PasswordHasher<ApplicationUser> hasher = new PasswordHasher<ApplicationUser>();

            modelBuilder.Entity<ApplicationUser>().HasData
            (
                new ApplicationUser
                {
                    Id = -2,
                    UserName = "xxxxx",
                    NormalizedUserName = "XXXXX",
                    PasswordHash = hasher.HashPassword(null, "xxxxx"),
                    SecurityStamp = securityStamp1,
                    EmailConfirmed = true
                },
                new ApplicationUser
                {
                    Id = -1,
                    UserName = "milutinbeslic",
                    NormalizedUserName = "MILUTINBESLIC",
                    NormalizedEmail = "MILUTINBESLIC@GMAIL.COM",
                    PasswordHash = hasher.HashPassword(null, "xxxxx"),
                    Email = "milutinbeslic@gmail.com",
                    SecurityStamp = securityStamp2,
                    EmailConfirmed = true,
                    PhotoPath = "~/pictures/M.svg"
                }
            );

            modelBuilder.Entity<IdentityRole<int>>().HasData
            (
                new IdentityRole<int>
                {
                    Id = 1,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new IdentityRole<int>
                {
                    Id = 2,
                    Name = "User",
                    NormalizedName = "USER"
                }
            );

            modelBuilder.Entity<IdentityUserRole<int>>().HasData
            (
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = -1
                },
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = -2
                }
            );
        }

        public static void SeedCategories(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData
            (
                new Category
                {
                    Id = 1,
                    Name = "Hobbies"
                },
                new Category
                {
                    Id = 2,
                    Name = "Social"
                },
                new Category
                {
                    Id = 3,
                    Name = "Video"
                },
                new Category
                {
                    Id = 4,
                    Name = "Arts"
                },
                new Category
                {
                    Id = 5,
                    Name = "Tech"
                },
                new Category
                {
                    Id = 6,
                    Name = "Gaming"
                },
                new Category
                {
                    Id = 7,
                    Name = "Science"
                },
                new Category
                {
                    Id = 8,
                    Name = "Exchange"
                },
                new Category
                {
                    Id = 9,
                    Name = "Pets"
                },
                new Category
                {
                    Id = 10,
                    Name = "Entertainment"
                },
                new Category
                {
                    Id = 11,
                    Name = "Education"
                },
                new Category
                {
                    Id = 12,
                    Name = "Q&As"
                },
                new Category
                {
                    Id = 13,
                    Name = "Politics"
                },
                new Category
                {
                    Id = 14,
                    Name = "Travel"
                }
            );
        }
    }
}
