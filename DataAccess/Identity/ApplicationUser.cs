﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Identity
{
    public class ApplicationUser : IdentityUser<int>
    {
        public string PhotoPath { get; set; }
        public int? PasswordRecoveryCode { get; set; }
        public DateTime? ConfirmationTokenExpiration { get; set; }
    }
}
