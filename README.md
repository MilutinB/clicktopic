# ClickTopic #

Messaging board CRUD app developed using ASP.NET Core framework, Entity framework Core, SignalR, JavaScript and JQuery.    

### Implemented features: ###

* CRUD operations for users, topics and replies
* User roles and logins - internal and external
* Email sending for new profile confirmation and password reset
* Background service for deleting uncorfimed users
* Topic bookmarking
* Notifications
* Searching and sorting
* Pagination
* Profile image uploading
