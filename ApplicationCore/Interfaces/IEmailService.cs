﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IEmailService
    {
        Task SendConfirmationEmailAsync(string address, string confirmationLink);
        Task SendPasswordVerificationCodeAsync(string address, int verificationCode);
        Task SendNewPasswordAsync(string address, string password);
    }
}
