﻿using ApplicationCore.ViewModels.Notification;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface INotificationViewModelService
    {
        Task<IReadOnlyList<NotificationViewModel>> GetUserNotificationsAsync(bool unseenOnly = false, bool firstFive = false);
    }
}
