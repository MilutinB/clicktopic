﻿using ApplicationCore.Entities;
using ApplicationCore.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface INotificationService
    {
        Task CreateNotificationAsync(Reply reply);
        Task<Result> ToggleTopicEnabledNotificationsAsync(int topicId);
        Task<bool> CheckIfEnabledNotificationsAsync(int topicId);
        Task DeleteAllEnabledNotificationsAsync(int topicId);
        Task<Result> SetNotificationsSeen();
        Task DeleteReplyNotificationsAsync(int replyId);
        Task DeleteTopicNotificationsAsync(IReadOnlyList<Reply> replies);
    }
}
