﻿using ApplicationCore.Helpers;
using ApplicationCore.ViewModels.Topic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface ITopicService
    {
        Task<Result> CreateNewTopicAsync(TopicCreateViewModel viewModel);
        Task<Result> ReplyToTopicAsync(TopicReplyViewModel viewModel);
        Task<Result> ReplyToCommentAsync(TopicReplyViewModel viewModel);
        Task<Result> DeleteReplyAsync(TopicDeleteViewModel viewModel);
        Task<Result> DeleteTopicAsync(TopicDeleteViewModel viewModel);
        Task IncreaseTopicViewCountAsync(int id);
    }
}
