﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface INotificationsSendService
    {
        Task SendNotificationsAsync(List<string> userIds, int topicId, int replyId, string dateCreated, string photoPath, string username, string topicTitle);
    }
}
