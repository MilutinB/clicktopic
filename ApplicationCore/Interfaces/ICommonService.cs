﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Interfaces
{
    public interface ICommonService
    {
        bool IsAjaxRequest();
        string GetJsonModelStateError(object modelStateDictionary);
        string GetJsonError(string message);
        string GetJsonSuccess(string returnUrl);
        void SetWelcomeCookie();
        bool CheckWelcomeCookie();
        void SetScrollToReplyCookie(int topicId, int replyId);
        void SetScrollToBottomCookie();
    }
}
