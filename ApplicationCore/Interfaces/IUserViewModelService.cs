﻿using ApplicationCore.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IUserViewModelService
    {
        UserViewModel GetUserViewModel(string returnUrl);
        Task<UserProfileViewModel> GetUserEditProfileViewModelAsync(string username);
    }
}
