﻿using ApplicationCore.Helpers;
using ApplicationCore.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Interfaces
{
    public interface ICommonViewModelService
    {
        ErrorViewModel GetErrorViewModel(Result result);
    }
}
