﻿using ApplicationCore.ViewModels.Topic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface ITopicViewModelService
    {
        Task<TopicCreateViewModel> GetCreateViewModelAsync();
        Task<TopicViewModel> GetTopicViewModelAsync(int id);
        Task<TopicIndexAggregateViewModel> GetTopicIndexViewModels(int page, string category = null, string order = null, string search = null);
        Task<MyTopicsAggregateViewModel> GetUserTopics(string username = null);
    }
}
