﻿using ApplicationCore.Helpers;
using ApplicationCore.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IUserService
    {
        Task<Result> LoginAsync(UserLoginViewModel viewModel);
        Task LogoutAsync();
        Task<Result> SignupAsync(UserSignupViewModel viewModel);
        Task<bool> IsUsernameTakenAsync(string username);
        Task<bool> IsEmailTakenAsync(string email);
        Task<Result> ConfirmedSignupAsync(string username, bool rememberMe, string token);
        Task DeleteExpiredTokenUsersAsync();
        Task SendResetVerificationCodeAsync(string email);
        Task ConfirmResetVerificationCodeAsync(int code);
        bool UserHasPermission(string username);
        Task<bool> UserIsAdmin(string username);
        Task<bool> CurrentUserIsAdminAsync();
        object GetExternalAuthenticationProperties(string provider, string returnUrl);
        Task<Result> ExternalLoginOrCreateUserAsync(string returnUrl);
        Task<Result> EditUsernameAsync(UserChangeUsernameViewModel viewModel);
        Task<Result> EditEmailAsync(UserChangeEmailViewModel viewModel);
        Task<Result> EditPasswordAsync(UserChangePasswordViewModel viewModel);
        Task<Result> DeleteProfileAsync(UserDeleteProfileViewModel viewModel);
        Task<Result> UploadProfilePhotoAsync(UserChangePhotoViewModel viewModel);
        Task<UserInfo> GetUserInfoAsync(string username);
        Task<UserInfo> GetUserInfoAsync(int id);
        Task<UserInfo> GetCurrentUserInfoAsync();
        bool UserIsLoggedIn();
        Task<IReadOnlyList<UserInfo>> GetAllUsersAsync();
        Task DeleteUserAsync(int id);
    }
}
