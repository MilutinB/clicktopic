﻿using ApplicationCore.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Interfaces
{
    public interface IBookmarkService
    {
        Task<bool> CheckIfBookmarkedAsync(int topicId);
        Task<Result> ToggleTopicBookmarksAsync(int topicId);
        Task DeleteAllTopicBookmarksAsync(int topicId);
    }
}
