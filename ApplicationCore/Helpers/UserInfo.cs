﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Helpers
{
    public class UserInfo
    {
        public int? Id { get; set; }
        public string Username { get; set; }
        public string PhotoPath { get; set; }
        public int TopicCount { get; set; }

        public override bool Equals(object obj)
        {
            UserInfo info = obj as UserInfo;
            return PhotoPath == info.PhotoPath
                && TopicCount == info.TopicCount
                && Username == info.Username
                && Id == info.Id;
        }
    }
}
