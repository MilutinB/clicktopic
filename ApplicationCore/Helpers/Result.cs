﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApplicationCore.Helpers
{
    public class Result
    {
        public Result(bool succeeded, string[] messages)
        {
            Succeeded = succeeded;
            Messages = messages;
        }

        public Result(bool succeeded, string[] messages, string returnUrl)
        {
            Succeeded = succeeded;
            Messages = messages;
            ReturnUrl = returnUrl;
        }

        public string ReturnUrl { get; }
        public bool Succeeded { get; }
        public string[] Messages { get; }
    }
}
