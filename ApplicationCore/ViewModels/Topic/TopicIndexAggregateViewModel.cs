﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.Topic
{
    public class TopicIndexAggregateViewModel
    {
        public IReadOnlyList<TopicIndexViewModel> TopicIndexViewModels { get; set; }
        public IReadOnlyList<string> Categories { get; set; }
        public string SelectedCategory { get; set; }
        public string OrderBy { get; set; }
        public int Page { get; set; }
        public int TotalPages { get; set; }
        public bool UserClosedWelcome { get; set; }
        public string SearchTerm { get; set; }
    }
}
