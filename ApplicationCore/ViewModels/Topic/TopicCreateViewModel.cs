﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationCore.ViewModels.Topic
{
    public class TopicCreateViewModel
    {
        [Required]
        [MaxLength(60, ErrorMessage = "Title cannot have more than 60 characters.")]
        public string Title { get; set; }

        [Required]
        [MaxLength(400, ErrorMessage = "Topic content cannot have more than 60 characters.")]
        public string Content { get; set; }

        [Required]
        public string Category { get; set; }
        public string Username { get; set; }
        public List<string> Categories { get; set; }
    }
}
