﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.Topic
{
    public class MyTopicsAggregateViewModel
    {
        public string Username { get; set; }
        public string PhotoPath { get; set; }
        public IReadOnlyList<TopicIndexViewModel> TopicIndexViewModels { get; set; }
        public IReadOnlyList<string> Categories { get; set; }
    }
}
