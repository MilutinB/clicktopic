﻿using ApplicationCore.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.Topic
{
    public class TopicIndexViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ViewCount { get; set; }
        public string ReplyCount { get; set; }
        public string Category { get; set; }
        public string LastActive { get; set; }
        public IReadOnlyList<UserInfo> UserInfos { get; set; }
    }
}
