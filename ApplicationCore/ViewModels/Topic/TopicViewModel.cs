﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.Topic
{
    public class TopicViewModel
    {
        public TopicReadViewModel TopicReadViewModel { get; set; }
        public TopicReplyViewModel TopicReplyViewModel { get; set; }
        public TopicDeleteViewModel TopicDeleteViewModel { get; set; }
    }
}
