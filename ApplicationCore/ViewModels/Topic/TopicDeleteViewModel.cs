﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.Topic
{
    public class TopicDeleteViewModel
    {
        public int TopicId { get; set; }
        public int CommentId { get; set; }
    }
}
