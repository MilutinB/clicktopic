﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationCore.ViewModels.Topic
{
    public class TopicReplyViewModel
    {
        public int TopicId { get; set; }
        public int? CommentId { get; set; }
        public string Username { get; set; }

        [Required(ErrorMessage = "Reply cannot be empty.")]
        [MaxLength(400, ErrorMessage = "Reply must be under 400 characters.")]
        public string Content { get; set; }
        public string PhotoPath { get; set; }
        public int UserTopicCount { get; set; }
        public DateTime DateCreated { get; set; }
        public TopicReplyViewModel RepliedToComment { get; set; }
        public IReadOnlyList<TopicReplyViewModel> CommentReplies { get; set; }
    }
}
