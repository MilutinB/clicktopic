﻿using ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.Topic
{
    public class TopicReadViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string TopicUsername { get; set; }
        public string LoggedinUsername { get; set; }
        public string PhotoPath { get; set; }
        public DateTime DateCreated { get; set; }
        public int UserTopicCount { get; set; }
        public bool UserFollowsTopic { get; set; }
        public bool UserEnabledNotificatons { get; set; }
        public IReadOnlyList<TopicReplyViewModel> TopicReplies { get; set; }
    }
}
