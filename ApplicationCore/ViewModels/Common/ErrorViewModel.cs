﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.Common
{
    public class ErrorViewModel
    {
        public string Title { get; set; }

        public string[] Messages { get; set; }
    }
}
