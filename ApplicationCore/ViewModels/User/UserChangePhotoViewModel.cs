﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.User
{
    public class UserChangePhotoViewModel
    {
        public string Username { get; set; }
        public string PhotoPath { get; set; }
        public string Base64 { get; set; }
    }
}
