﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.ViewModels.User
{
    public class UserViewModel
    {
        public UserLoginViewModel UserLoginViewModel { get; set; }
        public UserSignupViewModel UserSignupViewModel { get; set; }

    }
}
