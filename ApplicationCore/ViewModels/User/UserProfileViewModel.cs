﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.User
{
    public class UserProfileViewModel
    {
        public UserChangeEmailViewModel UserChangeEmailViewModel { get; set; }
        public UserChangeUsernameViewModel UserChangeUsernameViewModel { get; set; }
        public UserChangePhotoViewModel UserChangePhotoViewModel { get; set; }
        public UserChangePasswordViewModel UserChangePasswordViewModel { get; set; }
        public UserDeleteProfileViewModel UserDeleteProfileViewModel { get; set; }
    }
}
