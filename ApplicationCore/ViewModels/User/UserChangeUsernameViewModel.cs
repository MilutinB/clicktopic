﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationCore.ViewModels.User
{
    public class UserChangeUsernameViewModel
    {
        public string Username { get; set; }

        [Required(ErrorMessage = "The Username field is required.")]
        [RegularExpression(@"^[a-zA-Z\u017e\u017d\u0110\u0111\u0106\u0107\u010c\u010d\u0160\u0161][a-zA-Z0-9-_.+/\u017e\u017d\u0110\u0111\u0106\u0107\u010c\u010d\u0160\u0161]*$",
            ErrorMessage = "Must start with a letter. Can contain '-_.+/' special characters and numbers.")]
        [MaxLength(20, ErrorMessage = "Username cannot have more than 20 characters.")]
        public string NewUsername { get; set; }
    }
}
