﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationCore.ViewModels.User
{
    public class UserChangePasswordViewModel
    {
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "The Current Password field is required.")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "The New Password field is required.")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "The Confirmation field is required.")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Confirmation does not match.")]
        public string ConfirmPassword { get; set; }

        public bool IsExternal { get; set; }
    }
}
