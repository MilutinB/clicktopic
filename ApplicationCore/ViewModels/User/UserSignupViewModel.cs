﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.ViewModels.User
{
    public class UserSignupViewModel
    {
        [Required(ErrorMessage = "The Username field is required.")]
        [RegularExpression(@"^[a-zA-Z\u017e\u017d\u0110\u0111\u0106\u0107\u010c\u010d\u0160\u0161][a-zA-Z0-9-_.+/\u017e\u017d\u0110\u0111\u0106\u0107\u010c\u010d\u0160\u0161]*$",
            ErrorMessage = "Must start with a letter. Can contain '-_.+/' special characters and numbers.")]
        [MaxLength(20, ErrorMessage = "Username cannot have more than 20 characters.")]
        public string UserName { get; set; }

        [Required]
        [RegularExpression(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
            ErrorMessage = "Invalid Email address.")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirmation field is required.")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirmation does not match.")]
        public string ConfirmPassword { get; set; }

        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
