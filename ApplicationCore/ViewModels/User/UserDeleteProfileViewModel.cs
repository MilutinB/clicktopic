﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.User
{
    public class UserDeleteProfileViewModel
    {
        public string Username { get; set; }
    }
}
