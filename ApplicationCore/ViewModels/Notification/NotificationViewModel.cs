﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.ViewModels.Notification
{
    public class NotificationViewModel
    {
        public int NotificationCount { get; set; }
        public string Username { get; set; }
        public DateTime DateCreated { get; set; }
        public int TopicId { get; set; }
        public int? UserId { get; set; }
        public int? ReplyId { get; set; }
        public string PhotoPath { get; set; }
        public string TopicTitle { get; set; }
        public bool IsSeen { get; set; }
    }
}
