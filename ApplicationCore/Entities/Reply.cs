﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Reply
    {
        public int Id { get; set; }

        [MaxLength(400)]
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }

        public int? UserId { get; set; }

        [MaxLength(20)]
        public string Username { get; set; }
        public string PhotoPath { get; set; }

        public int TopicId { get; set; }
        public Topic Topic { get; set; }

        public int? RepliedToId { get; set; }
        public Reply RepliedTo { get; set; }
        public IReadOnlyList<Reply> Replies { get; set; }
    }
}
