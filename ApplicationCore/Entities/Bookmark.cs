﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Bookmark
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }

        public int TopicId { get; set; }
        public Topic Topic { get; set; }
    }
}
