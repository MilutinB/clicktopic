﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Topic
    {
        public int Id { get; set; }

        [MaxLength(60)]
        public string Title { get; set; }

        [MaxLength(400)]
        public string Content { get; set; }
        public DateTime DateCreated { get; set; }
        public int ViewCount { get; set; }

        public int? CategoryId { get; set; }
        public Category Category { get; set; }

        [MaxLength(20)]
        public string Username { get; set; }

        public ICollection<Reply> Replies { get; set; }
    }
}
