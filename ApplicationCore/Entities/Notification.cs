﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationCore.Entities
{
    public class Notification
    {
        public int Id { get; set; }

        [MaxLength(20)]
        public string Username { get; set; }
        public DateTime DateCreated { get; set; }
        public int TopicId { get; set; }
        public int? UserId { get; set; }
        public int? ReplyId { get; set; }
        public bool IsSeen { get; set; }
    }
}
