﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Entities
{
    public class EnabledNotification
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int TopicId { get; set; }
    }
}
