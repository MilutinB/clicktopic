﻿using ApplicationCore.Entities;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels.Topic;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Helpers;

namespace ApplicationCore.Services
{
    public class TopicService : ITopicService
    {
        private readonly ITopicRepository topicRepository;
        private readonly ICategoryRepository categoryRepository;
        private readonly ICommonService commonService;
        private readonly IReplyRepository replyRepository;
        private readonly IUserService userService;
        private readonly IBookmarkService bookmarkService;
        private readonly INotificationService notificationService;

        public TopicService(ITopicRepository topicRepository,
                            ICategoryRepository categoryRepository,
                            ICommonService commonService,
                            IReplyRepository replyRepository,
                            IUserService userService,
                            IBookmarkService bookmarkService,
                            INotificationService notificationService)
        {
            this.topicRepository = topicRepository;
            this.categoryRepository = categoryRepository;
            this.commonService = commonService;
            this.replyRepository = replyRepository;
            this.userService = userService;
            this.bookmarkService = bookmarkService;
            this.notificationService = notificationService;
        }

        public async Task<Result> CreateNewTopicAsync(TopicCreateViewModel viewModel)
        {
            Category category = await categoryRepository.GetSingleAsync(c => c.Name == viewModel.Category);
            Topic topic = new Topic
            {
                Title = TrimTopicContent(viewModel.Title),
                Content = TrimTopicContent(viewModel.Content),
                CategoryId = category.Id,
                Username = viewModel.Username,
                DateCreated = DateTime.Now,
                ViewCount = -1
            };

            try
            {
                await topicRepository.AddAsync(topic);
                await topicRepository.SaveChangesAsync();
                await bookmarkService.ToggleTopicBookmarksAsync(topic.Id);
                await notificationService.ToggleTopicEnabledNotificationsAsync(topic.Id);
            }
            catch (Exception)
            {
                return new Result(false, new string[] { "We had issues creating your topic. Please try again." });
            }
            return new Result(true, new string[] { topic.Id.ToString() });
        }

        public async Task<Result> ReplyToTopicAsync(TopicReplyViewModel viewModel)
        {
            Reply reply = new Reply
            {
                TopicId = viewModel.TopicId,
                Content = viewModel.Content,
                DateCreated = DateTime.Now,
                Username = viewModel.Username
            };
            try
            {
                await replyRepository.AddAsync(reply);
                await replyRepository.SaveChangesAsync();
                await notificationService.CreateNotificationAsync(reply);
                commonService.SetScrollToBottomCookie();
            }
            catch (Exception ex)
            {
                return new Result(false, new string[] { "We had issues adding your reply. Please try again." });
            }
            return new Result(true, new string[] { reply.TopicId.ToString() });
        }

        public async Task<Result> ReplyToCommentAsync(TopicReplyViewModel viewModel)
        {
            Reply reply = new Reply
            {
                TopicId = viewModel.TopicId,
                Content = viewModel.Content,
                DateCreated = DateTime.Now,
                Username = viewModel.Username,
                RepliedToId = viewModel.CommentId
            };
            try
            {
                await replyRepository.AddAsync(reply);
                await replyRepository.SaveChangesAsync();
                await notificationService.CreateNotificationAsync(reply);
                commonService.SetScrollToBottomCookie();
            }
            catch (Exception)
            {
                return new Result(false, new string[] { "We had issues adding your reply. Please try again." });
            }
            return new Result(true, new string[] { reply.TopicId.ToString() });
        }

        public async Task<Result> DeleteReplyAsync(TopicDeleteViewModel viewModel)
        {
            Reply reply = await replyRepository.GetByIdAsync(viewModel.CommentId);

            if (reply != null)
            {
                if (userService.UserHasPermission(reply.Username) || await userService.CurrentUserIsAdminAsync())
                {
                    int replyId = reply.Id;
                    replyRepository.Delete(reply);

                    try
                    {
                        await replyRepository.SaveChangesAsync();
                        await notificationService.DeleteReplyNotificationsAsync(replyId);
                        return new Result(true, new string[] { viewModel.TopicId.ToString() });
                    }
                    catch (Exception ex)
                    {
                        return new Result(false, new string[] { "We had issues with your request." });
                    }
                }
                return new Result(false, new string[] { "You're not permitted to do this." });
            }
            return new Result(false, new string[] { "This comment no longer exists." });
        }

        public async Task<Result> DeleteTopicAsync(TopicDeleteViewModel viewModel)
        {
            Topic topic = await topicRepository.GetSingleAsync(t => t.Id == viewModel.TopicId, t => t.Replies);

            if (topic != null)
            {
                if (userService.UserHasPermission(topic.Username) || await userService.CurrentUserIsAdminAsync())
                {
                    IReadOnlyList<Reply> replies = topic.Replies.ToList();
                    topicRepository.Delete(topic);

                    try
                    {
                        await topicRepository.SaveChangesAsync();
                        await bookmarkService.DeleteAllTopicBookmarksAsync(viewModel.TopicId);
                        await notificationService.DeleteAllEnabledNotificationsAsync(viewModel.TopicId);

                        if (replies.Count > 0)
                        {
                            await notificationService.DeleteTopicNotificationsAsync(replies);
                        }
                        return new Result(true, null);
                    }
                    catch (Exception)
                    {
                        return new Result(false, new string[] { "We had issues with your request." });
                    }
                }
                return new Result(false, new string[] { "You're not permitted to do this." });
            }
            return new Result(false, new string[] { "This topic no longer exists." });
        }

        public async Task IncreaseTopicViewCountAsync(int id)
        {
            Topic topic = await topicRepository.GetByIdAsync(id);

            if (topic != null)
            {
                try
                {
                    ++topic.ViewCount;
                    await topicRepository.SaveChangesAsync();
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        private string TrimTopicContent(string content)
        {
            StringBuilder builder = new StringBuilder(content);
            int count = 0;
            while (content[count] == ' ')
            {
                count++;
            }
            string frontClean = null;

            if (count > 0)
            {
                frontClean = builder.Remove(0, count).ToString();
            }
            else
            {
                frontClean = content;
            }

            count = frontClean.Length - 1;
            while (frontClean[count] == ' ')
            {
                count--;
            }
            count = frontClean.Length - 1 - count;
            string backClean = null;
            builder = new StringBuilder(frontClean);

            if (count > 0)
            {
                backClean = builder.Remove(frontClean.Length - count, count).ToString();
            }
            else
            {
                backClean = content;
            }

            return backClean;
        }
    }
}
