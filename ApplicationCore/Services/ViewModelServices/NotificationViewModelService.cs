﻿using ApplicationCore.Entities;
using ApplicationCore.Helpers;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels.Notification;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services.ViewModelServices
{
    public class NotificationViewModelService : INotificationViewModelService
    {
        private readonly INotificationRepository notificationRepository;
        private readonly IUserService userService;
        private readonly ITopicRepository topicRepository;

        public NotificationViewModelService(INotificationRepository notificationRepository, 
                                            IUserService userService, 
                                            ITopicRepository topicRepository)
        {
            this.notificationRepository = notificationRepository;
            this.userService = userService;
            this.topicRepository = topicRepository;
        }


        public async Task<IReadOnlyList<NotificationViewModel>> GetUserNotificationsAsync(bool unseenOnly, bool firstFive)
        {
            UserInfo currentInfo = await userService.GetCurrentUserInfoAsync();
            IReadOnlyList<Notification> notifications = null;

            //var notdel = await notificationRepository.GetAllAsync();
            //foreach (var item in notdel)
            //{
            //    notificationRepository.Delete(item);
            //}
            //await notificationRepository.SaveChangesAsync();

            if (unseenOnly)
            {
                notifications = await notificationRepository.GetAsync(n => n.UserId == currentInfo.Id && !n.IsSeen, n => n.DateCreated, true);
            }
            else if (firstFive)
            {
                notifications = await notificationRepository.GetAsync(n => n.UserId == currentInfo.Id && n.IsSeen, n => n.DateCreated, true, 0, 5);
            }
            else
            {
                notifications = await notificationRepository.GetAsync(n => n.UserId == currentInfo.Id, n => n.DateCreated, true);
            }
            List<NotificationViewModel> viewModels = new List<NotificationViewModel>();

            if (notifications != null || notifications.Count > 0)
            {
                foreach (Notification notification in notifications)
                {
                    UserInfo info = await userService.GetUserInfoAsync(notification.Username);
                    Topic topic = await topicRepository.GetByIdAsync(notification.TopicId);

                    NotificationViewModel viewModel = new NotificationViewModel
                    {
                        DateCreated = notification.DateCreated,
                        IsSeen = notification.IsSeen,
                        TopicTitle = topic.Title,
                        ReplyId = notification.ReplyId,
                        TopicId = notification.TopicId,
                        Username = info.Username,
                        PhotoPath = info.PhotoPath
                    };
                    viewModels.Add(viewModel);
                }
            }
            return viewModels;
        }
    }
}
