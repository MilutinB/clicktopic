﻿using ApplicationCore.Helpers;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Services.ViewModelServices
{
    public class CommonViewModelService : ICommonViewModelService
    {
        public ErrorViewModel GetErrorViewModel(Result result)
        {
            return new ErrorViewModel
            {
                Title = "Error",
                Messages = result.Messages
            };
        }
    }
}
