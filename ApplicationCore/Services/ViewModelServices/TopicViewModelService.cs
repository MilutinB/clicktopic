﻿using ApplicationCore.Entities;
using ApplicationCore.Helpers;
using ApplicationCore.Interfaces;
using ApplicationCore.ViewModels.Topic;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace ApplicationCore.Services.ViewModelServices
{
    public class TopicViewModelService : ITopicViewModelService
    {
        private readonly ICategoryRepository categoryRepository;
        private readonly ITopicRepository topicRepository;
        private readonly IUserService userService;
        private readonly IReplyRepository replyRepository;
        private readonly ICommonService commonService;
        private readonly IBookmarkService bookmarkService;
        private readonly IBookmarkRepository bookmarkRepository;
        private readonly IHttpContextAccessor accessor;
        private readonly INotificationService notificationService;
        private readonly int pageSize = 20;

        public TopicViewModelService(ICategoryRepository categoryRepository,
                                     ITopicRepository topicRepository,
                                     IUserService userService,
                                     IReplyRepository replyRepository,
                                     ICommonService commonService,
                                     IBookmarkService bookmarkService,
                                     IBookmarkRepository bookmarkRepository,
                                     IHttpContextAccessor accessor,
                                     INotificationService notificationService)
        {
            this.categoryRepository = categoryRepository;
            this.topicRepository = topicRepository;
            this.userService = userService;
            this.replyRepository = replyRepository;
            this.commonService = commonService;
            this.bookmarkService = bookmarkService;
            this.bookmarkRepository = bookmarkRepository;
            this.accessor = accessor;
            this.notificationService = notificationService;
        }

        public async Task<TopicCreateViewModel> GetCreateViewModelAsync()
        {
            IReadOnlyList<Category> categories = await categoryRepository.GetAllAsync();
            List<string> vmCategories = new List<string>();

            foreach (Category category in categories)
            {
                vmCategories.Add(category.Name);
            }

            return new TopicCreateViewModel
            {
                Categories = vmCategories
            };
        }

        public async Task<TopicViewModel> GetTopicViewModelAsync(int id)
        {
            Topic topic = await topicRepository.GetByIdAsync(id);
            TopicViewModel viewModel = null;

            if (topic != null)
            {
                UserInfo info = new UserInfo();
                IReadOnlyList<Reply> replies = await replyRepository.GetAsync(r => r.TopicId == topic.Id, r => r.Replies);
                List<TopicReplyViewModel> topicReplyViewModels = new List<TopicReplyViewModel>();

                foreach (Reply reply in replies)
                {
                    info = await userService.GetUserInfoAsync(reply.Username);
                    string photopath = info.PhotoPath;
                    int topicCount = info.TopicCount;
                    TopicReplyViewModel repliedToComment = null;

                    if (reply.RepliedToId != null)
                    {
                        Reply repliedTo = await replyRepository.GetByIdAsync(reply.RepliedToId);
                        info = await userService.GetUserInfoAsync(repliedTo.Username);
                        repliedToComment = new TopicReplyViewModel
                        {
                            CommentId = repliedTo.Id,
                            Content = repliedTo.Content,
                            Username = repliedTo.Username,
                            PhotoPath = info.PhotoPath
                        };
                    }
                    List<TopicReplyViewModel> commentReplyViewModels = null;

                    if (reply.Replies.Count > 0)
                    {
                        commentReplyViewModels = new List<TopicReplyViewModel>();

                        foreach (Reply commentReply in reply.Replies)
                        {
                            info = await userService.GetUserInfoAsync(commentReply.Username);
                            TopicReplyViewModel commentReplyViewModel = new TopicReplyViewModel
                            {
                                CommentId = commentReply.Id,
                                Content = commentReply.Content,
                                Username = commentReply.Username,
                                PhotoPath = info.PhotoPath
                            };
                            commentReplyViewModels.Add(commentReplyViewModel);
                        }
                    }
                    TopicReplyViewModel topicReplyViewModel = new TopicReplyViewModel
                    {
                        CommentId = reply.Id,
                        Content = reply.Content,
                        Username = reply.Username,
                        PhotoPath = photopath,
                        UserTopicCount = topicCount,
                        RepliedToComment = repliedToComment,
                        CommentReplies = commentReplyViewModels,
                        DateCreated = reply.DateCreated
                    };
                    topicReplyViewModels.Add(topicReplyViewModel);
                }
                info = await userService.GetUserInfoAsync(topic.Username);

                bool userFollowsTopic = false;
                bool userEnabledNotifications = false;

                if (userService.UserIsLoggedIn())
                {
                    userFollowsTopic = await bookmarkService.CheckIfBookmarkedAsync(topic.Id);
                    userEnabledNotifications = await notificationService.CheckIfEnabledNotificationsAsync(topic.Id);
                }

                viewModel = new TopicViewModel
                {
                    TopicReadViewModel = new TopicReadViewModel
                    {
                        Id = topic.Id,
                        Title = topic.Title,
                        Content = topic.Content,
                        DateCreated = topic.DateCreated,
                        TopicUsername = topic.Username,
                        PhotoPath = info.PhotoPath,
                        UserTopicCount = info.TopicCount,
                        TopicReplies = topicReplyViewModels,
                        UserFollowsTopic = userFollowsTopic,
                        UserEnabledNotificatons = userEnabledNotifications
                    }
                };
            }
            return viewModel;
        }

        public async Task<TopicIndexAggregateViewModel> GetTopicIndexViewModels(int page, string category = null, string order = null, string search = null)
        {
            IReadOnlyList<Topic> topics = null;

            if (page < 1)
            {
                page = 1;
            }

            if (category != null || search == null)
            {
                topics = await GetTopicsWithCategory(category, order, page);
            }
            else
            {
                topics = await GetTopicsWithSearch(search);
            }
            IReadOnlyList<Topic> topicsToCount = null;

            if (order == "Follow")
            {
                topicsToCount = topics;
            }
            else if (category == null && search == null)
            {
                topicsToCount = await topicRepository.GetAllAsync();
            }
            else
            {
                topicsToCount = await topicRepository.GetAsync(t => t.Category.Name == category);
            }

            int totalPages = (int)Math.Ceiling((double)topicsToCount.Count / pageSize);
            IReadOnlyList<Category> categories = await categoryRepository.GetAllAsync();
            bool userClosedWelcome = commonService.CheckWelcomeCookie();
            List<TopicIndexViewModel> viewModels = null;

            TopicIndexAggregateViewModel aggregateViewModel = new TopicIndexAggregateViewModel
            {
                Categories = categories.Select(c => c.Name).ToList(),
                SelectedCategory = category,
                OrderBy = order,
                Page = page,
                TotalPages = totalPages,
                UserClosedWelcome = userClosedWelcome,
                SearchTerm = search
            };

            if (topics != null)
            {
                viewModels = new List<TopicIndexViewModel>();

                foreach (Topic topic in topics)
                {
                    List<UserInfo> userInfos = await GetUserInfos(topic.Username, topic.Replies);
                    string replyCount = GetNormalizedCount(topic.Replies.Count);
                    string viewCount = GetNormalizedCount(topic.ViewCount);
                    string lastActive = GetLastActive(topic.DateCreated, topic.Replies);

                    TopicIndexViewModel viewModel = new TopicIndexViewModel
                    {
                        Id = topic.Id,
                        Category = topic.Category.Name,
                        Title = topic.Title,
                        ReplyCount = replyCount,
                        ViewCount = viewCount,
                        LastActive = lastActive,
                        UserInfos = userInfos
                    };
                    viewModels.Add(viewModel);
                }
                aggregateViewModel.TopicIndexViewModels = viewModels;
            }
            return aggregateViewModel;
        }

        private async Task<IReadOnlyList<Topic>> GetTopicsWithCategory(string category, string order, int page)
        {
            IReadOnlyList<Topic> topics = null;
            int skip = (page - 1) * pageSize;

            if (category == null)
            {
                switch (order)
                {
                    case "Latest":
                        topics = await topicRepository.GetAsync(t => t.DateCreated, true, skip, pageSize, t => t.Category, t => t.Replies);
                        break;
                    case "Top":
                        topics = await topicRepository.GetAsync(t => t.Replies.Count, true, skip, pageSize, t => t.Category, t => t.Replies);
                        break;
                    case "Follow":
                        UserInfo userInfo = await userService.GetUserInfoAsync(accessor.HttpContext.User.Identity.Name);
                        IReadOnlyList<Bookmark> bookmarks = await bookmarkRepository.GetAsync(b => b.UserId == userInfo.Id, b => b.Id, true, skip, pageSize, b => b.Topic.Replies);

                        if (bookmarks.Count > 0)
                        {
                            List<Topic> bookmarkedTopics = new List<Topic>();

                            foreach (Bookmark bookmark in bookmarks)
                            {
                                bookmarkedTopics.Add(bookmark.Topic);
                            }
                            topics = bookmarkedTopics.OrderByDescending(t => t.DateCreated).ToList();
                        }
                        else
                        {
                            topics = new List<Topic>();
                        }
                        break;
                    default:
                        topics = await topicRepository.GetAsync(t => t.DateCreated, true, skip, pageSize, t => t.Category, t => t.Replies);
                        break;
                }
            }
            else
            {
                switch (order)
                {
                    case "Latest":
                        topics = await topicRepository.GetAsync(t => t.Category.Name == category, t => t.DateCreated, true, skip, pageSize, t => t.Replies);
                        break;
                    case "Top":
                        topics = await topicRepository.GetAsync(t => t.Category.Name == category, t => t.Replies.Count, true, skip, pageSize, t => t.Replies);
                        break;
                    default:
                        topics = await topicRepository.GetAsync(t => t.Category.Name == category, t => t.DateCreated, true, skip, pageSize, t => t.Replies);
                        break;
                }
            }
            return topics;
        }

        private async Task<IReadOnlyList<Topic>> GetTopicsWithSearch(string search)
        {
            return await topicRepository.GetAsync(t => t.Title.ToLower().Contains(search.ToLower()), t => t.DateCreated, true, 0, 50, t => t.Replies);
        }

        public async Task<MyTopicsAggregateViewModel> GetUserTopics(string username)
        {
            List<TopicIndexViewModel> viewModels = null;
            UserInfo info = null;
            bool userIsLoggedIn = userService.UserIsLoggedIn();
            IReadOnlyList<Category> categories = await categoryRepository.GetAllAsync();

            MyTopicsAggregateViewModel aggregateViewModel = new MyTopicsAggregateViewModel
            {
                Categories = categories.Select(c => c.Name).ToList(),
            };

            if (username == null && userIsLoggedIn)
            {
                info = await userService.GetCurrentUserInfoAsync();
            }
            else if (username != null)
            {
                info = await userService.GetUserInfoAsync(username);
            }
            if (info.Username == null)
            {
                return aggregateViewModel;
            }
            aggregateViewModel.PhotoPath = info.PhotoPath;
            aggregateViewModel.Username = info.Username;
            IReadOnlyList<Topic> topics = await topicRepository.GetAsync(t => t.Username == info.Username, t => t.DateCreated, true, t => t.Replies);

            if (topics != null || topics.Count > 0)
            {
                viewModels = new List<TopicIndexViewModel>();

                foreach (Topic topic in topics)
                {
                    List<UserInfo> userInfos = await GetUserInfos(topic.Username, topic.Replies);
                    string replyCount = GetNormalizedCount(topic.Replies.Count);
                    string viewCount = GetNormalizedCount(topic.ViewCount);
                    string lastActive = GetLastActive(topic.DateCreated, topic.Replies);

                    TopicIndexViewModel viewModel = new TopicIndexViewModel
                    {
                        Id = topic.Id,
                        Category = topic.Category.Name,
                        Title = topic.Title,
                        ReplyCount = replyCount,
                        ViewCount = viewCount,
                        LastActive = lastActive,
                        UserInfos = userInfos
                    };
                    viewModels.Add(viewModel);
                }
                aggregateViewModel.TopicIndexViewModels = viewModels;
            }
            else
            {
                aggregateViewModel.TopicIndexViewModels = new List<TopicIndexViewModel>();
            }
            return aggregateViewModel;
        }

        private async Task<List<UserInfo>> GetUserInfos(string username, ICollection<Reply> replies)
        {
            UserInfo userInfo = await userService.GetUserInfoAsync(username);
            List<UserInfo> userInfos = new List<UserInfo> { userInfo };

            if (replies != null)
            {
                int count = 0;

                foreach (Reply reply in replies)
                {
                    if (count == 3)
                    {
                        break;
                    }
                    userInfo = await userService.GetUserInfoAsync(reply.Username);

                    if (!userInfos.Contains(userInfo))
                    {
                        userInfos.Add(userInfo);
                        count++;
                    }
                }
            }
            return userInfos;
        }

        private string GetNormalizedCount(int count)
        {
            if (count == 0)
            {
                return "0";
            }

            if (count > 1000)
            {
                string result = "";
                string number = count.ToString();
                int shortCount = count.ToString().Length - 2;

                for (int i = 0; i < shortCount; i++)
                {
                    if (i == shortCount - 1)
                    {
                        result += ".";
                        result += number[i];
                    }
                    else
                    {
                        result += number[i];
                    }
                }
                result += "k";
                return result;
            }
            return count.ToString();
        }

        private string GetLastActive(DateTime dateTopicCreated, ICollection<Reply> replies)
        {
            DateTime? dateToUse = null;

            if (replies.Count > 0)
            {
                dateToUse = replies.Last().DateCreated;
            }
            else
            {
                dateToUse = dateTopicCreated;
            }

            TimeSpan span = DateTime.Now - dateToUse.Value;

            if (span.TotalMinutes < 60)
            {
                return span.TotalMinutes.ToString().Split('.')[0] + "m";
            }
            if (span.TotalHours < 24)
            {
                return span.TotalHours.ToString().Split('.')[0] + "h";
            }
            if (span.TotalDays < 30)
            {
                return span.TotalDays.ToString().Split('.')[0] + "d";
            }
            if (span.TotalDays > 30 && span.TotalDays < 365)
            {
                return (span.TotalDays / 30).ToString().Split('.')[0] + "mo";
            }
            return (span.TotalDays / 365).ToString().Split('.')[0] + "y";
        }
    }
}
