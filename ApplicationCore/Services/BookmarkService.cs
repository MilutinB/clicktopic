﻿using ApplicationCore.Entities;
using ApplicationCore.Helpers;
using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class BookmarkService : IBookmarkService
    {
        private readonly IBookmarkRepository bookmarkRepository;
        private readonly IUserService userService;
        private readonly IHttpContextAccessor accessor;

        public BookmarkService(IBookmarkRepository bookmarkRepository, IUserService userService, IHttpContextAccessor accessor)
        {
            this.bookmarkRepository = bookmarkRepository;
            this.userService = userService;
            this.accessor = accessor;
        }

        public async Task<Result> ToggleTopicBookmarksAsync(int topicId)
        {
            UserInfo info = await userService.GetUserInfoAsync(accessor.HttpContext.User.Identity.Name);
            Bookmark bookmark = await bookmarkRepository.GetSingleAsync(b => b.UserId == info.Id.Value && b.TopicId == topicId);

            if (bookmark == null)
            {
                bookmark = new Bookmark
                {
                    UserId = info.Id.Value,
                    TopicId = topicId,
                    DateCreated = DateTime.Now
                };
                try
                {
                    await bookmarkRepository.AddAsync(bookmark);
                    await bookmarkRepository.SaveChangesAsync();
                    return new Result(true, null);
                }
                catch (Exception)
                {
                    return new Result(false, null);
                }
            }

            try
            {
                bookmarkRepository.Delete(bookmark);
                await bookmarkRepository.SaveChangesAsync();
                return new Result(true, null);
            }
            catch (Exception)
            {
                return new Result(false, null);
            }
        }

        public async Task<bool> CheckIfBookmarkedAsync(int topicId)
        {
            if (accessor.HttpContext.User.Identity.Name == null)
            {
                return false;
            }
            UserInfo info = await userService.GetUserInfoAsync(accessor.HttpContext.User.Identity.Name);
            Bookmark bookmark = await bookmarkRepository.GetSingleAsync(b => b.UserId == info.Id && b.TopicId == topicId);
            return bookmark != null;
        }


        public async Task DeleteAllTopicBookmarksAsync(int topicId)
        {
            IReadOnlyList<Bookmark> bookmarks = await bookmarkRepository.GetAsync(t => t.TopicId == topicId);

            if (bookmarks != null || bookmarks.Count > 0)
            {
                foreach (Bookmark bookmark in bookmarks)
                {
                    bookmarkRepository.Delete(bookmark);
                }

                try
                {
                    await bookmarkRepository.SaveChangesAsync();
                }
                catch (Exception)
                {
                    //
                }
            }
        }
    }
}
