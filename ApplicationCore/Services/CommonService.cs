﻿using ApplicationCore.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ApplicationCore.Services
{
    public class CommonService : ICommonService
    {
        private readonly IHttpContextAccessor accessor;

        public CommonService(IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
        }

        public string GetJsonSuccess(string returnUrl)
        {
            return JsonConvert.SerializeObject(new
            {
                Succedded = true,
                ReturnUrl = returnUrl
            });
        }

        public string GetJsonError(string message)
        {
            return JsonConvert.SerializeObject(new
            {
                Succedded = false,
                Message = message
            });
        }

        public string GetJsonModelStateError(object modelStateDictionary)
        {
            ModelStateDictionary modelState = modelStateDictionary as ModelStateDictionary;
            return JsonConvert.SerializeObject(new
            {
                Message = modelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).First()
            });
        }

        public bool IsAjaxRequest()
        {
            if (accessor.HttpContext.Request.Headers.ContainsKey("X-Requested-With"))
            {
                StringValues values = new StringValues();
                accessor.HttpContext.Request.Headers.TryGetValue("X-Requested-With", out values);
                if (values.Contains("XMLHttpRequest"))
                {
                    return true;
                }
            }
            return false;
        }

        public void SetWelcomeCookie()
        {
            CookieOptions cookieOptions = new CookieOptions { Expires = DateTime.Now.AddDays(3650) };
            accessor.HttpContext.Response.Cookies.Append("CLICK_TOPIC", "DONT_SHOW_WELCOME", cookieOptions);
        }

        public bool CheckWelcomeCookie()
        {
            string cookieValue = accessor.HttpContext.Request.Cookies["CLICK_TOPIC"];

            if (cookieValue != null)
            {
                return cookieValue == "DONT_SHOW_WELCOME";
            }
            return false;
        }

        public void SetScrollToReplyCookie(int topicId, int replyId)
        {
            CookieOptions cookieOptions = new CookieOptions { Expires = DateTime.Now.AddDays(3650), HttpOnly = false };
            accessor.HttpContext.Response.Cookies.Append("CLICK_TOPIC_SCROLL_TO_REPLY", topicId.ToString() + "-" + replyId.ToString(), cookieOptions);
        }

        public void SetScrollToBottomCookie()
        {
            CookieOptions cookieOptions = new CookieOptions { Expires = DateTime.Now.AddDays(3650), HttpOnly = false };
            accessor.HttpContext.Response.Cookies.Append("CLICK_TOPIC_SCROLL_TO_BOTTOM", "yes", cookieOptions);
        }
    }
}
