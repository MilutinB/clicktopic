﻿using ApplicationCore.Entities;
using ApplicationCore.Helpers;
using ApplicationCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository notificationRepository;
        private readonly IUserService userService;
        private readonly IEnabledNotificationRepository enabledNotificationRepository;
        private readonly INotificationsSendService notificationsSendService;
        private readonly ITopicRepository topicRepository;

        public NotificationService(INotificationRepository notificationRepository,
                                   IUserService userService,
                                   IEnabledNotificationRepository enabledNotificationRepository,
                                   INotificationsSendService notificationsSendService,
                                   ITopicRepository topicRepository)
        {
            this.notificationRepository = notificationRepository;
            this.userService = userService;
            this.enabledNotificationRepository = enabledNotificationRepository;
            this.notificationsSendService = notificationsSendService;
            this.topicRepository = topicRepository;
        }

        public async Task CreateNotificationAsync(Reply reply)
        {
            IReadOnlyList<EnabledNotification> enabledNotifications = await enabledNotificationRepository.GetAsync(b => b.TopicId == reply.TopicId);

            if (enabledNotifications != null || enabledNotifications.Count > 0)
            {
                List<string> userIds = new List<string>();

                foreach (EnabledNotification enabledNotification in enabledNotifications)
                {
                    UserInfo userInfo = await userService.GetUserInfoAsync(enabledNotification.UserId);

                    if (userInfo.Username != reply.Username)
                    {
                        Notification notification = new Notification
                        {
                            DateCreated = reply.DateCreated,
                            IsSeen = false,
                            ReplyId = reply.Id,
                            TopicId = reply.TopicId,
                            Username = reply.Username,
                            UserId = enabledNotification.UserId
                        };
                        await notificationRepository.AddAsync(notification);
                        userIds.Add(userInfo.Id.ToString());
                    }
                }
                UserInfo info = await userService.GetUserInfoAsync(reply.Username);
                Topic topic = await topicRepository.GetByIdAsync(reply.TopicId);
                try
                {
                    await notificationRepository.SaveChangesAsync();
                    await notificationsSendService.SendNotificationsAsync(userIds, reply.TopicId, reply.Id, reply.DateCreated.Date.ToString().Split(" ")[0], info.PhotoPath, info.Username, topic.Title);
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        public async Task<bool> CheckIfEnabledNotificationsAsync(int topicId)
        {
            UserInfo info = await userService.GetCurrentUserInfoAsync();

            if (info == null)
            {
                return false;
            }
            EnabledNotification enabledNotification = await enabledNotificationRepository.GetSingleAsync(b => b.UserId == info.Id.Value && b.TopicId == topicId);
            return enabledNotification != null;
        }

        public async Task<Result> ToggleTopicEnabledNotificationsAsync(int topicId)
        {
            UserInfo info = await userService.GetCurrentUserInfoAsync();
            EnabledNotification enabledNotification = await enabledNotificationRepository.GetSingleAsync(e => e.UserId == info.Id.Value && e.TopicId == topicId);

            if (enabledNotification == null)
            {
                enabledNotification = new EnabledNotification
                {
                    UserId = info.Id.Value,
                    TopicId = topicId
                };
                try
                {
                    await enabledNotificationRepository.AddAsync(enabledNotification);
                    await enabledNotificationRepository.SaveChangesAsync();
                    return new Result(true, null);
                }
                catch (Exception)
                {
                    return new Result(false, null);
                }
            }

            try
            {
                enabledNotificationRepository.Delete(enabledNotification);
                await enabledNotificationRepository.SaveChangesAsync();
                return new Result(true, null);
            }
            catch (Exception)
            {
                return new Result(false, null);
            }
        }

        public async Task DeleteAllEnabledNotificationsAsync(int topicId)
        {
            IReadOnlyList<EnabledNotification> enabledNotifications = await enabledNotificationRepository.GetAsync(e => e.TopicId == topicId);

            if (enabledNotifications != null)
            {
                foreach (EnabledNotification enabledNotification in enabledNotifications)
                {
                    enabledNotificationRepository.Delete(enabledNotification);
                }

                try
                {
                    await enabledNotificationRepository.SaveChangesAsync();
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        public async Task DeleteAllNotifications(string username)
        {
            IReadOnlyList<Notification> notifications = await notificationRepository.GetAsync(n => n.Username == username);

            if (notifications != null || notifications.Count > 0)
            {
                foreach (Notification notification in notifications)
                {
                    notificationRepository.Delete(notification);
                }
                try
                {
                    await notificationRepository.SaveChangesAsync();
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        public async Task DeleteTopicNotificationsAsync(IReadOnlyList<Reply> replies)
        {
            foreach (Reply reply in replies)
            {
                Notification notification = await notificationRepository.GetSingleAsync(n => n.ReplyId == reply.Id);

                if (notification != null)
                {
                    notificationRepository.Delete(notification);
                }
            }
            try
            {
                await notificationRepository.SaveChangesAsync();
            }
            catch (Exception)
            {
                //
            }
        }

        public async Task<Result> SetNotificationsSeen()
        {
            UserInfo userInfo = await userService.GetCurrentUserInfoAsync();
            IReadOnlyList<Notification> notifications = await notificationRepository.GetAsync(n => n.UserId == userInfo.Id && !n.IsSeen);

            if (notifications != null || notifications.Count > 0)
            {
                foreach (Notification notification in notifications)
                {
                    notification.IsSeen = true;
                }
                try
                {
                    await notificationRepository.SaveChangesAsync();
                    return new Result(true, null);
                }
                catch (Exception)
                {
                    return new Result(false, null);
                }
            }
            return new Result(false, null);
        }

        public async Task DeleteReplyNotificationsAsync(int replyId)
        {
            IReadOnlyList<Notification> notifications = await notificationRepository.GetAsync(n => n.ReplyId == replyId);

            if (notifications.Count > 0)
            {
                foreach (Notification notification in notifications)
                {
                    notificationRepository.Delete(notification);

                    try
                    {
                        await notificationRepository.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        //
                    }
                }
            }
        }
    }
}
